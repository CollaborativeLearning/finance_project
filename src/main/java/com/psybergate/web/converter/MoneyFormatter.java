package com.psybergate.web.converter;

import java.math.BigDecimal;
import java.text.DecimalFormat;

import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.convert.Converter;
import javax.faces.convert.FacesConverter;

import com.psybergate.finance.domain.Money;

@FacesConverter("com.psybergate.web.converter.MoneyFormatter")
public class MoneyFormatter implements Converter {

	@Override
	public Object getAsObject(FacesContext context, UIComponent component, String value) {
		if (value == null || value.trim().equals("")) {
			value = "0.00";
		}
		Money money = new Money(new BigDecimal(value));
		return money;
	}

	@Override
	public String getAsString(FacesContext context, UIComponent component, Object value) {
		Money money = (Money) value;
		return new DecimalFormat("#,##0.00").format(money.getAmount());
	}

}
