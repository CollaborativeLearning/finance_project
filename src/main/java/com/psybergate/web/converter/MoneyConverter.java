package com.psybergate.web.converter;

import java.math.BigDecimal;

import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.convert.Converter;
import javax.faces.convert.FacesConverter;

import com.psybergate.finance.domain.Money;

@FacesConverter("com.psybergate.web.converter.MoneyConverter")
public class MoneyConverter implements Converter {

	@Override
	public Object getAsObject(FacesContext context, UIComponent component, String value) {
		if (value == null || value.trim().equals("")) {
			return null;
		}
		return new Money(new BigDecimal(value));
	}

	@Override
	public String getAsString(FacesContext context, UIComponent component, Object value) {
		if (value == null) {
			return "";
		}
		Money money = (Money) value;
		return money.getAmount().toString();
	}

}
