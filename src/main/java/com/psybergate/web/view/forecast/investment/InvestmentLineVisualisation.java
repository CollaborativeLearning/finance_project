package com.psybergate.web.view.forecast.investment;

import java.math.BigDecimal;
import java.math.RoundingMode;

import javax.enterprise.context.RequestScoped;
import javax.inject.Named;

import org.primefaces.model.chart.AxisType;
import org.primefaces.model.chart.LineChartModel;
import org.primefaces.model.chart.LineChartSeries;

import com.psybergate.finance.interfaces.ForecastEntry;
import com.psybergate.finance.view.model.InvestmentViewModel;

@RequestScoped
@Named
public class InvestmentLineVisualisation {

	public BigDecimal getPrincipalTermAsBigDecimal(
			InvestmentViewModel investmentviewModel) {
		return BigDecimal.valueOf(investmentviewModel.getTerm());
	}

	public LineChartModel getModel(InvestmentViewModel investmentViewModel) {
		LineChartModel model = new LineChartModel();

		LineChartSeries capitalGainPoints = new LineChartSeries();
		try {

			for (ForecastEntry forecastEntry : investmentViewModel.getForecast()) {
				capitalGainPoints.set(forecastEntry.getMonth(),
						forecastEntry.getClosingBalance().getAmount());
			}

			model.getAxis(AxisType.Y).setMin(-1);
			model.getAxis(AxisType.Y)
					.setMax(investmentViewModel.getForecast()
							.get(investmentViewModel.getForecast().size() - 1)
							.getClosingBalance().getAmount()
							.add(getAverageOpeningBalances(investmentViewModel)));
		} catch (Exception e) {
		}
		model.getAxis(AxisType.Y).setLabel("Principal (R)");

		model.getAxis(AxisType.X).setMin(0);
		model.getAxis(AxisType.X).setLabel("Month");

		model.addSeries(capitalGainPoints);
		model.setTitle("Principal Growth Over Time");
		return model;
	}

	private BigDecimal getAverageOpeningBalances(
			InvestmentViewModel investmentViewModel) {
		BigDecimal average = BigDecimal.ZERO;

		for (ForecastEntry forecastEntry : investmentViewModel.getForecast()) {
			average = average.add(forecastEntry.getClosingBalance().getAmount());
		}

		return average.divide(BigDecimal.valueOf(investmentViewModel.getTerm()),
				2, RoundingMode.HALF_UP);
	}

}
