package com.psybergate.web.view.forecast.propertybond;

import java.math.BigDecimal;
import java.math.RoundingMode;

import javax.enterprise.context.RequestScoped;
import javax.inject.Named;

import org.primefaces.model.chart.AxisType;
import org.primefaces.model.chart.LineChartModel;
import org.primefaces.model.chart.LineChartSeries;

import com.psybergate.finance.interfaces.ForecastEntry;
import com.psybergate.finance.view.model.PropertyBondViewModel;

@Named
@RequestScoped
public class PropertyBondLineVisualisation {

	public BigDecimal getPrincipalTermAsBigDecimal(PropertyBondViewModel PropertyBondViewModel) {
		return BigDecimal.valueOf(PropertyBondViewModel.getTerm());
	}

	public LineChartModel getModel(PropertyBondViewModel propertyBondViewModel) {
		LineChartModel model = new LineChartModel();

		LineChartSeries capitalGainPoints = new LineChartSeries();

		for (int i = propertyBondViewModel.getForecast().size(); -1 < --i;) {
			capitalGainPoints.set(propertyBondViewModel.getForecast().get(i).getMonth(),
					propertyBondViewModel.getForecast().get(i).getClosingBalance().getAmount());
		}

		model.getAxis(AxisType.Y).setMin(getAverageOpeningBalances(propertyBondViewModel).multiply(BigDecimal.valueOf(-2.8)));
		model.getAxis(AxisType.Y).setMax(propertyBondViewModel.getForecast().get(0).getClosingBalance().getAmount()
				.add(getAverageOpeningBalances(propertyBondViewModel).multiply(BigDecimal.valueOf(2.8))));
		model.getAxis(AxisType.Y).setLabel("Repayment (R)");

		model.getAxis(AxisType.X).setMin(0);
		model.getAxis(AxisType.X).setLabel("Month");

		model.addSeries(capitalGainPoints);
		model.setTitle("Repayment Over Time");
		return model;
	}

	private BigDecimal getAverageOpeningBalances(PropertyBondViewModel propertyBondViewModel) {
		BigDecimal average = BigDecimal.ZERO;

		for (ForecastEntry forecastEntry : propertyBondViewModel.getForecast()) {
			average = average.add(forecastEntry.getClosingBalance().getAmount());
		}

		return average.divide(BigDecimal.valueOf(propertyBondViewModel.getTerm() * propertyBondViewModel.getTerm()), 2,
				RoundingMode.HALF_UP);
	}

}
