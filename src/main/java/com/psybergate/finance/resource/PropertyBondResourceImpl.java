package com.psybergate.finance.resource;

import java.util.List;

import javax.enterprise.context.RequestScoped;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.TypedQuery;

import com.psybergate.finance.domain.PropertyBond;
import com.psybergate.finance.domain.PropertyBondEvent;
import com.psybergate.finance.interfaces.PropertyBondResource;

@RequestScoped
public class PropertyBondResourceImpl implements PropertyBondResource {

	@PersistenceContext(unitName = "financeDatabase")
	private EntityManager entityManager;

	public void savePropertyBond(PropertyBond propertyBond) {
		entityManager.persist(propertyBond);
	}

	public void savePropertyBond(PropertyBond propertyBond,
			List<PropertyBondEvent> events) {
		savePropertyBond(propertyBond);
		for (PropertyBondEvent event : events) {
			event.setPropertyBondId(propertyBond.getId());
			entityManager.persist(event);
		}
	}

	public List<PropertyBond> getSavedPropertyBonds(Integer customerNum) {
		TypedQuery<PropertyBond> query = entityManager.createQuery(
				"SELECT r FROM PropertyBond AS r WHERE r.customer.customerNum = :customerNum",
				PropertyBond.class);
		query.setParameter("customerNum", customerNum);
		List<PropertyBond> resultList = query.getResultList();
		return resultList;
	}

	public List<PropertyBondEvent> getPropertyBondEvents(
			Integer propertyBondId) {
		TypedQuery<PropertyBondEvent> query = entityManager.createQuery(
				"SELECT e FROM Event AS e WHERE e.forecastId = :forecastId",
				PropertyBondEvent.class);
		query.setParameter("forecastId", propertyBondId);
		return query.getResultList();
	}

}
