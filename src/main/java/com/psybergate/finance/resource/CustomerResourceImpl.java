package com.psybergate.finance.resource;

import java.util.List;

import javax.enterprise.context.RequestScoped;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.TypedQuery;

import com.psybergate.finance.domain.Customer;
import com.psybergate.finance.interfaces.CustomerResource;
import com.psybergate.finance.util.PrintUtil;

@RequestScoped
public class CustomerResourceImpl implements CustomerResource {

	@PersistenceContext(unitName = "financeDatabase")
	private EntityManager entityManager;

	@Override
	public void addCustomer(Customer customer) {
		PrintUtil.print("CustomerResourceImpl, saving customer: " + customer);
		entityManager.persist(customer);
	}

	@Override
	public Customer getCustomer(Integer customerNum) {
		String query = "SELECT cust FROM Customer AS cust WHERE cust.customerNum = :customerNum";
		TypedQuery<Customer> typedQuery = entityManager.createQuery(query,
				Customer.class);
		typedQuery.setParameter("customerNum", customerNum);
		List<Customer> resultList = typedQuery.getResultList();
		if (resultList.isEmpty()) {
			return null;
		}
		return resultList.get(0);
	}
}