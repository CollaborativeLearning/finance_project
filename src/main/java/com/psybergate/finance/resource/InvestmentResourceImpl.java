package com.psybergate.finance.resource;

import java.util.List;

import javax.enterprise.context.RequestScoped;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.TypedQuery;

import com.psybergate.finance.domain.Investment;
import com.psybergate.finance.domain.InvestmentEvent;
import com.psybergate.finance.interfaces.InvestmentResource;

@RequestScoped
public class InvestmentResourceImpl implements InvestmentResource {

	@PersistenceContext(unitName = "financeDatabase")
	private EntityManager entityManager;

	@Override
	public void saveInvestment(Investment investment) {
		entityManager.persist(investment);
	}

	@Override
	public List<Investment> getInvestments(Integer customerId) {
		TypedQuery<Investment> query = entityManager.createQuery(
				"SELECT investment FROM Investment AS investment WHERE investment.customerId = :customerId",
				Investment.class);
		query.setParameter("customerId", customerId);
		List<Investment> resultList = query.getResultList();
		for (Investment investment : resultList) {
			List<InvestmentEvent> events = getInvestmentEvents(investment.getId());
			investment.setEvents(events);
		}
		return resultList;
	}

	@Override
	public List<InvestmentEvent> getInvestmentEvents(Integer investmentId) {
		TypedQuery<InvestmentEvent> query = entityManager.createQuery(
				"SELECT e FROM InvestmentEvent AS e WHERE e.investmentId = :investmentId",
				InvestmentEvent.class);
		query.setParameter("investmentId", investmentId);
		return query.getResultList();
	}

	@Override
	public Investment getInvestment(Integer investmentId) {
		return entityManager.find(Investment.class, investmentId);
	}

	@Override
	public void addEvent(Integer investmentId, InvestmentEvent event) {
		Investment investment = getInvestment(investmentId);
		if (investment != null) {
			event.setInvestmentId(investmentId);
			entityManager.persist(event);
		}
	}

	@Override
	public void updateInvestment(Investment investment) {
		entityManager.merge(investment);
	}

}
