package com.psybergate.finance.service;

import java.math.BigDecimal;
import java.util.List;

import javax.enterprise.context.ApplicationScoped;
import javax.transaction.Transactional;

import com.psybergate.finance.domain.Customer;
import com.psybergate.finance.domain.PropertyBond;
import com.psybergate.finance.domain.PropertyBondEvent;
import com.psybergate.finance.interfaces.PropertyBondService;
import com.psybergate.finance.view.model.PropertyBondDataViewModel;

@ApplicationScoped
public class PropertyBondServiceImpl extends PropertyBondValidatorServiceImpl
		implements
			PropertyBondService {

//	@Inject
//	private PropertyBondResource propertyBondResource;

	@Override
	public PropertyBond generatePopertyBond(
			PropertyBondDataViewModel propertyBondData) {
		PropertyBond bond = new PropertyBond(propertyBondData.getTerm(),
				propertyBondData.getInterestRate(), propertyBondData.getPrincipal(),
				propertyBondData.getEvents());
		return bond;
	}

	@Override
	@Transactional
	public void savePropertyBond(Customer customer, String propertyBondName,
			BigDecimal principal, Integer term, Double interestRate,
			List<PropertyBondEvent> events) {
		// Validation happens here. Validate inputs, validate events.
		// Once validation passes, proceed with persistence.
	}

	@Override
	public List<PropertyBond> getSavedPropertyBonds(Integer customerNum) {
		return null;
	}

	public List<PropertyBondEvent> getPropertyBondEvents(
			Integer propertyBondId) {
		return null;
	}

}
