package com.psybergate.finance.service;

import javax.enterprise.context.RequestScoped;
import javax.inject.Inject;
import javax.transaction.Transactional;

import com.psybergate.finance.domain.Customer;
import com.psybergate.finance.interfaces.CustomerService;
import com.psybergate.finance.resource.CustomerResourceImpl;

@RequestScoped
public class CustomerServiceImpl implements CustomerService {

	@Inject
	private CustomerResourceImpl customerResource;

	@Override
	@Transactional
	public void addCustomer(Customer customer) {
		customerResource.addCustomer(customer);
	}

	@Override
	public Customer getCustomer(Integer customerNum) {
		return customerResource.getCustomer(customerNum);
	}

}
