package com.psybergate.finance.service;

import java.util.List;

import javax.enterprise.context.ApplicationScoped;
import javax.inject.Inject;
import javax.transaction.Transactional;

import com.psybergate.finance.domain.Investment;
import com.psybergate.finance.domain.InvestmentEvent;
import com.psybergate.finance.domain.Money;
import com.psybergate.finance.interfaces.InvestmentResource;
import com.psybergate.finance.interfaces.InvestmentService;
import com.psybergate.finance.util.PrintUtil;

@ApplicationScoped
public class InvestmentServiceImpl extends InvestmentValidatorServiceImpl
		implements
			InvestmentService {

	@Inject
	private InvestmentResource investmentResource;

	@Override
	@Transactional
	public void saveInvestment(Investment investment) {
		validateInvestment(investment, true);
		Integer id = investment.getId();
		if (id != null) {
			Investment existingInvestment = getInvestment(id);
			if (existingInvestment != null) {
				PrintUtil.print("updating already existing investment: "
						+ existingInvestment);
				updateInvestment(investment);
				return;
			}
		}
		investmentResource.saveInvestment(investment);
	}

	@Override
	public void updateInvestment(Investment investment) {
		investmentResource.updateInvestment(investment);
	}

	@Transactional
	private void saveInvestment(Integer customerId, String investmentName,
			Money principal, Integer term, Double interestRate,
			Money monthlyContribution, List<InvestmentEvent> events) {
		// Validation happens here. Validate inputs, validate events.
		// Once validation passes, proceed with persistence.
		Investment investment = new Investment(customerId, investmentName,
				principal, interestRate, term, monthlyContribution, events);
		investmentResource.saveInvestment(investment);
	}

	@Transactional
	@Override
	public List<Investment> getInvestments(Integer customerId) {
		return investmentResource.getInvestments(customerId);
	}

	public Investment getInvestment(Integer investmentId) {
		Investment investment = investmentResource.getInvestment(investmentId);
		return investment;
	}

	@Override
	public List<InvestmentEvent> getInvestmentEvents(Integer investmentId) {
		PrintUtil.print("getting events for investment: " + investmentId);
		List<InvestmentEvent> events = investmentResource
				.getInvestmentEvents(investmentId);
		PrintUtil.print("events found: " + events);
		return events;
	}

	@Override
	@Transactional
	public void addEvent(Investment investment, InvestmentEvent event) {
		Integer investmentId = investment.getId();
		addEvent(investmentId, event);
	}

	@Override
	@Transactional
	public void addEvent(Integer investmentId, InvestmentEvent event) {
		investmentResource.addEvent(investmentId, event);
		PrintUtil
				.print("adding event " + event + " to investment: " + investmentId);
	}

}