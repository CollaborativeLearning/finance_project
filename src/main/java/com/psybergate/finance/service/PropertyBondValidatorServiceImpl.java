package com.psybergate.finance.service;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.psybergate.finance.domain.PropertyBondEvent;
import com.psybergate.finance.domain.Money;
import com.psybergate.finance.domain.PropertyBond;
import com.psybergate.finance.exception.ApplicationException;
import com.psybergate.finance.exception.ValidationException;
import com.psybergate.finance.interfaces.ForecastEntry;
import com.psybergate.finance.interfaces.PropertyBondValidatorService;
import com.psybergate.finance.util.PropertyBondCalculatorUtil;

public class PropertyBondValidatorServiceImpl
		implements
			PropertyBondValidatorService {

	public static final Money MINIMUM_MONTHLY_REPAYMENT_FOR_PROPERTY_BOND = new Money(
			0);

	public static final Money MINIMUM_PRINCIPAL_FOR_PROPERTY_BOND = new Money(1);

	public static final Integer MINIMUM_TERM_FOR_PROPERTY_BOND = 1;

	public static final Integer MINIMUM_INTEREST_RATE_FOR_PROPERTY_BOND = 1;

	public static final Integer MAXIMUM_INTEREST_RATE_FOR_PROPERTY_BOND = 100;

	@Override
	public void validateEvent(PropertyBond propertyBond, PropertyBondEvent event)
			throws ValidationException {
		Map<String, String> errorMessages = new HashMap<>();
		try {
			validateMonth(propertyBond, event);
		} catch (ApplicationException e) {
			errorMessages.put("month", e.getMessage());
		}
		try {
			validateInterestRate(event);
		} catch (ApplicationException e) {
			errorMessages.put("interestRate", e.getMessage());
		}
		try {
			validateAmount(propertyBond, event);
		} catch (ApplicationException e) {
			errorMessages.put("amount", e.getMessage());
		}
		try {
			validateMonthlyRepayment(propertyBond, event);
		} catch (ApplicationException e) {
			errorMessages.put("monthlyAmount", e.getMessage());
		}

		if (!errorMessages.isEmpty()) {
			throw new ValidationException(errorMessages);
		}
	}

	private void validateMonth(PropertyBond propertyBond, PropertyBondEvent event) {
		Integer month = event.getMonth();
		Integer maxMonth = propertyBond.getMonthSettled();
		if (maxMonth == null) {
			maxMonth = propertyBond.getTerm();
		}
		if (month == null || month < 1 || month > maxMonth) {
			String message = String.format(
					"month must be between %s and %s (inclusive)", 1, maxMonth);
			throw new ApplicationException(message);
		}
	}

	private void validateMonthlyRepayment(PropertyBond propertyBond,
			PropertyBondEvent event) {
		Map<String, String> errorMessages = new HashMap<>();
		if (errorMessages.containsKey("month")) {
			throw new ApplicationException(
					"this value depends on month, please specify a valid month first");
		}
		if (errorMessages.containsKey("interestRate")) {
			throw new ApplicationException(
					"this value depends on interest rate, please specify a valid interest rate first");
		}
		if (!event.monthlyAmountChanged()) return;
		ForecastEntry entry = propertyBond.getForecast()
				.get(event.getMonth() - 1);
		Integer newTerm = propertyBond.getTerm() - event.getMonth() + 1;
		Money newMonthlyRepayment = PropertyBondCalculatorUtil
				.getNewMonthlyRepayment(entry, event, newTerm);

		Money monthlyRepayment = event.getMonthlyAmount();
		if (monthlyRepayment.compareTo(newMonthlyRepayment) < 0) {
			String message = String.format(
					"monthly contribution should not be less than %s",
					newMonthlyRepayment);
			throw new ApplicationException(message);
		}
	}

	private void validateAmount(PropertyBond propertyBond, PropertyBondEvent event) {
		if (!event.additionalAmountChanged()) return;
		try {
			validateMonth(propertyBond, event);
		} catch (Exception e) {
			throw new ApplicationException(
					"this value depends on month, please specify a valid month first");
		}
		Integer month = event.getMonth();
		Money amount = event.getAmount();
		Money openingBalance = propertyBond.getForecast().get(month - 1)
				.getOpeningBalance();
		if (openingBalance.add(amount).getAmount().doubleValue() < 0) {
			String message = String.format("maximum withdrawal amount is %s",
					openingBalance);
			throw new ApplicationException(message);
		}
	}

	private void validateInterestRate(PropertyBondEvent event) {
		if (!event.interestRateChanged()) return;
		Double interestRate = event.getInterestRate();
		if (interestRate < MINIMUM_INTEREST_RATE_FOR_PROPERTY_BOND
				|| interestRate > MAXIMUM_INTEREST_RATE_FOR_PROPERTY_BOND) {
			String message = String.format(
					"Interest rate should be in range (%s, %s)",
					MINIMUM_INTEREST_RATE_FOR_PROPERTY_BOND,
					MAXIMUM_INTEREST_RATE_FOR_PROPERTY_BOND);
			throw new ApplicationException(message);
		}
	}

	@Override
	public void validatePropertyBond(PropertyBond propertyBond,
			boolean validateEvents) throws ValidationException {
		Map<String, String> errorMessages = new HashMap<>();
		try {
			validatePrincipal(propertyBond.getPrincipal());
		} catch (ApplicationException e) {
			errorMessages.put("principal", e.getMessage());
		}
		try {
			validateTerm(propertyBond.getTerm());
		} catch (ApplicationException e) {
			errorMessages.put("term", e.getMessage());
		}
		try {
			validateInterestRate(propertyBond.getInterestRate());
		} catch (ApplicationException e) {
			errorMessages.put("interestRate", e.getMessage());
		}

		if (!errorMessages.isEmpty()) {
			throw new ValidationException(errorMessages);
		}
		if (validateEvents) {
			validateEvents(propertyBond);
		}
	}

	@Override
	public void validateEvents(PropertyBond propertyBond) {
		validateEvents(propertyBond, propertyBond.getEvents());
	}

	@Override
	public void validateEvents(PropertyBond propertyBond,
			List<PropertyBondEvent> events) {
		for (PropertyBondEvent event : events) {
			validateEvent(propertyBond, event);
		}
	}

	private void validatePrincipal(Money principal) {
		if (principal == null) {
			throw new ApplicationException("principal should not be empty");
		}
		if (principal.compareTo(MINIMUM_PRINCIPAL_FOR_PROPERTY_BOND) < 0) {
			String message = String.format("principal should not be less than %s",
					MINIMUM_PRINCIPAL_FOR_PROPERTY_BOND);
			throw new ApplicationException(message);
		}
	}

	private void validateTerm(Integer term) {
		if (term == null) {
			throw new ApplicationException("term should not be empty");
		}
		if (term < MINIMUM_TERM_FOR_PROPERTY_BOND) {
			String message = String.format("term should not be less than %s",
					MINIMUM_TERM_FOR_PROPERTY_BOND);
			throw new ApplicationException(message);
		}
	}

	private void validateInterestRate(Double interestRate) {
		if (interestRate == null) {
			throw new ApplicationException("interest should not be empty");
		}
		if (interestRate < MINIMUM_INTEREST_RATE_FOR_PROPERTY_BOND
				|| interestRate > MAXIMUM_INTEREST_RATE_FOR_PROPERTY_BOND) {
			String message = String.format(
					"Interest rate should be in range (%s, %s)",
					MINIMUM_INTEREST_RATE_FOR_PROPERTY_BOND,
					MAXIMUM_INTEREST_RATE_FOR_PROPERTY_BOND);
			throw new ApplicationException(message);
		}
	}

}