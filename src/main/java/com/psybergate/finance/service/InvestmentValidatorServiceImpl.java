package com.psybergate.finance.service;

import java.util.HashMap;
import java.util.Map;

import com.psybergate.finance.domain.Investment;
import com.psybergate.finance.domain.InvestmentEvent;
import com.psybergate.finance.domain.Money;
import com.psybergate.finance.exception.ApplicationException;
import com.psybergate.finance.exception.ValidationException;
import com.psybergate.finance.interfaces.InvestmentValidatorService;
import com.psybergate.finance.util.PrintUtil;

public class InvestmentValidatorServiceImpl
		implements
			InvestmentValidatorService {

	public static final Money MINIMUM_PRINCIPAL_FOR_INVESTMENT = new Money(1);

	public static final Integer MINIMUM_TERM_FOR_INVESTMENT = 1;

	public static final Integer MAXIMUM_TERM_FOR_INVESTMENT = 720;

	public static final Integer MINIMUM_INTEREST_RATE_FOR_INVESTMENT = 1;

	public static final Integer MAXIMUM_INTEREST_RATE_FOR_INVESTMENT = 100;

	public static final Money MINIMUM_MONTHLY_CONTRIBUTION_FOR_INVESTMENT = new Money(
			0);

	@Override
	public void validateInvestment(Investment investment,
			boolean validateEvents) {
		PrintUtil.print("validating investment:" + investment);
		Map<String, String> messages = new HashMap<String, String>();
		try {
			validatePrincipal(investment.getPrincipal());
		} catch (ApplicationException e) {
			messages.put("principal", e.getMessage());
		}
		try {
			validateTerm(investment.getTerm());
		} catch (ApplicationException e) {
			messages.put("term", e.getMessage());
		}
		try {
			validateInterestRate(investment.getInterestRate());
		} catch (ApplicationException e) {
			messages.put("interestRate", e.getMessage());
		}
		try {
			validateMonthlyContribution(investment.getMonthlyContribution());
		} catch (ApplicationException e) {
			messages.put("monthlyContribution", e.getMessage());
		}

		if (!messages.isEmpty()) {
			throw new ValidationException(messages);
		}

		if (validateEvents && investment.hasEvents()) {
			PrintUtil.print("validating events: " + investment.getEvents());
			validateEvents(investment);
		}
	}

	@Override
	public void validateEvents(Investment investment) {
		for (InvestmentEvent event : investment.getEvents()) {
			validateEvent(investment, event);
		}
	}

	private void validatePrincipal(Money principal) {
		if (principal == null) {
			throw new ApplicationException("principal should not be empty");
		}
		if (principal.compareTo(MINIMUM_PRINCIPAL_FOR_INVESTMENT) < 0) {
			String message = String.format("principal should not be less than %s",
					MINIMUM_PRINCIPAL_FOR_INVESTMENT);
			throw new ApplicationException(message);
		}
	}

	private void validateTerm(Integer term) {
		if (term == null) {
			throw new ApplicationException("term should not be empty");
		}
		if (term < MINIMUM_TERM_FOR_INVESTMENT) {
			String message = String.format("term should not be less than %s",
					MINIMUM_TERM_FOR_INVESTMENT);
			throw new ApplicationException(message);
		}
		if (term > MAXIMUM_TERM_FOR_INVESTMENT) {
			String message = String.format("term should exceed %s",
					MINIMUM_TERM_FOR_INVESTMENT);
			throw new ApplicationException(message);
		}
	}

	private void validateInterestRate(Double interestRate) {
		if (interestRate == null) {
			throw new ApplicationException("interest rate should not be empty");
		}
		if (interestRate < MINIMUM_INTEREST_RATE_FOR_INVESTMENT
				|| interestRate > MAXIMUM_INTEREST_RATE_FOR_INVESTMENT) {
			String message = String.format(
					"Interest rate should be in range (%s, %s)",
					MINIMUM_INTEREST_RATE_FOR_INVESTMENT,
					MAXIMUM_INTEREST_RATE_FOR_INVESTMENT);
			throw new ApplicationException(message);
		}
	}

	private void validateMonthlyContribution(Money monthlyContribution) {
		if (monthlyContribution == null) {
			return;
		}
		if (monthlyContribution
				.compareTo(MINIMUM_MONTHLY_CONTRIBUTION_FOR_INVESTMENT) < 0) {
			String message = String.format(
					"monthly contribution should not be less than %s",
					MINIMUM_MONTHLY_CONTRIBUTION_FOR_INVESTMENT);
			throw new ApplicationException(message);
		}
	}

	// EVENT VALIDATION

	@Override
	public void validateEvent(Investment investment, InvestmentEvent event) {
		PrintUtil.print("validating event: " + event);
		Map<String, String> errorMessages = new HashMap<>();
		try {
			validateEventMonth(investment, event);
		} catch (ApplicationException e) {
			errorMessages.put("month", e.getMessage());
		}
		try {
			validateEventMonthlyContribution(event);
		} catch (ApplicationException e) {
			errorMessages.put("monthlyContribution", e.getMessage());
		}
		try {
			validateEventAdditionalAmount(investment, event);
		} catch (ApplicationException e) {
			errorMessages.put("amount", e.getMessage());
		}
		try {
			validateEventInterestRate(event);
		} catch (ApplicationException e) {
			errorMessages.put("interestRate", e.getMessage());
		}
		PrintUtil.print("event validation messages: " + errorMessages);
		if (!errorMessages.isEmpty()) {
			throw new ValidationException(errorMessages);
		}
	}

	private void validateEventMonth(Investment investment,
			InvestmentEvent event) {
		Integer term = investment.getTerm();
		if (term == null) {
			throw new ApplicationException("term cannot be empty");
		}
		Integer month = event.getMonth();
		if (month == null || month < 1 || month > term) {
			throw new ApplicationException(
					String.format("month must be in range (%s, %s)", 1, term));
		}
	}

	private void validateEventInterestRate(InvestmentEvent event) {
		if (!event.interestRateChanged()) return;
		Double interestRate = event.getInterestRate();
		if (interestRate == null
				|| interestRate < MINIMUM_INTEREST_RATE_FOR_INVESTMENT
				|| interestRate > MAXIMUM_INTEREST_RATE_FOR_INVESTMENT) {
			String message = String.format(
					"Interest rate should be in range (%s, %s)",
					MINIMUM_INTEREST_RATE_FOR_INVESTMENT,
					MAXIMUM_INTEREST_RATE_FOR_INVESTMENT);
			throw new ApplicationException(message);
		}
	}

	private void validateEventAdditionalAmount(Investment investment,
			InvestmentEvent event) {
		if (!event.additionalAmountChanged()) return;
		Integer month = event.getMonth();
		Money amount = event.getAmount();
		try {
			validateEventMonth(investment, event);
		} catch (ApplicationException e) {
			throw new ApplicationException(
					"this value depends on month, please provide valid month");
		}
		if (amount.getAmount().doubleValue() < 0) {
			Money maxWithdrawable = investment.getForecast().get(month - 1)
					.getOpeningBalance();
			if (amount.add(maxWithdrawable).getAmount().doubleValue() < 0) {
				String message = String.format("maximum withdrawal amount is %s",
						maxWithdrawable);
				throw new ApplicationException(message);
			}

		}
	}

	private void validateEventMonthlyContribution(InvestmentEvent event) {
		if (!event.monthlyAmountChanged()) return;
		Money monthlyContribution = event.getMonthlyContribution();
		if (monthlyContribution == null || (monthlyContribution
				.compareTo(MINIMUM_MONTHLY_CONTRIBUTION_FOR_INVESTMENT) < 0)) {
			String message = String.format(
					"monthly contribution should not be less than %s",
					MINIMUM_MONTHLY_CONTRIBUTION_FOR_INVESTMENT);
			throw new ApplicationException(message);
		}
	}

}
