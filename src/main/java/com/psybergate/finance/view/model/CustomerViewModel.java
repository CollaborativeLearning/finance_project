package com.psybergate.finance.view.model;

import java.io.Serializable;

import javax.enterprise.context.RequestScoped;
import javax.inject.Named;

@RequestScoped
@Named
public class CustomerViewModel implements Serializable {

	private static final long serialVersionUID = 6943058772266676542L;

	private Integer id;

	private Integer customerNum;

	private String name;

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public Integer getCustomerNum() {
		return customerNum;
	}

	public void setCustomerNum(Integer customerNum) {
		this.customerNum = customerNum;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	@Override
	public String toString() {
		return "CustomerViewModel [id=" + id + ", customerNum=" + customerNum
				+ ", name=" + name + "]";
	}

}
