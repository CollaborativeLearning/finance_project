package com.psybergate.finance.view.model;

import javax.enterprise.context.RequestScoped;
import javax.inject.Named;

@RequestScoped
@Named("forecastInfo")
public class ForecastInfoViewModelImpl implements ForecastInfoViewModel {

	private Integer customerNum;

	private String customerName;

	private String forecastName;

	public ForecastInfoViewModelImpl() {
	}

	public ForecastInfoViewModelImpl(Integer customerNum, String customerName, String forecastName) {
		this.customerNum = customerNum;
		this.customerName = customerName;
		this.forecastName = forecastName;
	}

	public ForecastInfoViewModelImpl(Integer customerNum, String investmentName) {
		this(customerNum, "", investmentName);
	}

	@Override
	public Integer getCustomerNum() {
		return customerNum;
	}

	@Override
	public void setCustomerNum(Integer customerNum) {
		this.customerNum = customerNum;
	}

	@Override
	public String getCustomerName() {
		return customerName;
	}

	@Override
	public void setCustomerName(String customerName) {
		this.customerName = customerName;
	}

	@Override
	public String getForecastName() {
		return forecastName;
	}

	@Override
	public void setForecastName(String forecastName) {
		this.forecastName = forecastName;
	}

}
