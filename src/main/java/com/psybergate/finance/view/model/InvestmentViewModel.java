package com.psybergate.finance.view.model;

import java.io.Serializable;
import java.util.List;

import javax.enterprise.inject.spi.CDI;
import javax.faces.view.ViewScoped;
import javax.inject.Named;

import com.psybergate.finance.domain.InvestmentForecastEntry;
import com.psybergate.finance.domain.Money;
import com.psybergate.finance.util.PrintUtil;

@ViewScoped
@Named
public class InvestmentViewModel implements Serializable {

	private static final long serialVersionUID = 2605546133147882749L;

	private Integer id;

	private String name;

	private Money principal;

	private Integer term;

	private Double interestRate;

	private Money monthlyContribution;

	private Money closingBalance;

	private Money futureValue;

	private Money totalInterest;

	private Money totalContribution;

	private List<EventViewModel> events;

	private List<InvestmentForecastEntry> forecast;

	public InvestmentViewModel() {
		InvestmentDataViewModel data = CDI.current()
				.select(InvestmentDataViewModel.class).get();
		setId(data.getId());
		setPrincipal(data.getPrincipal());
		setTerm(data.getTerm());
		setInterestRate(data.getInterestRate());
		setMonthlyContribution(data.getMonthlyContribution());
		setEvents(data.getEvents());
		PrintUtil.print("investment data: " + data);
	}

	public Money getTotalContribution() {
		return totalContribution;
	}

	public void setTotalContribution(Money totalContribution) {
		this.totalContribution = totalContribution;
	}

	public Money getTotalInterest() {
		return totalInterest;
	}

	public void setTotalInterest(Money totalInterest) {
		this.totalInterest = totalInterest;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public Money getPrincipal() {
		return principal;
	}

	public void setPrincipal(Money principal) {
		this.principal = principal;
	}

	public Integer getTerm() {
		return term;
	}

	public void setTerm(Integer term) {
		this.term = term;
	}

	public Double getInterestRate() {
		return interestRate;
	}

	public void setInterestRate(Double interestRate) {
		this.interestRate = interestRate;
	}

	public Money getMonthlyContribution() {
		return monthlyContribution;
	}

	public void setMonthlyContribution(Money monthlyContribution) {
		this.monthlyContribution = monthlyContribution;
	}

	public Money getClosingBalance() {
		return closingBalance;
	}

	public void setClosingBalance(Money closingBalance) {
		this.closingBalance = closingBalance;
	}

	public Money getFutureValue() {
		return futureValue;
	}

	public void setFutureValue(Money futureValue) {
		this.futureValue = futureValue;
	}

	public List<EventViewModel> getEvents() {
		return events;
	}

	public void setEvents(List<EventViewModel> events) {
		this.events = events;
	}

	public List<InvestmentForecastEntry> getForecast() {
		return forecast;
	}

	public void setForecast(List<InvestmentForecastEntry> forecast) {
		this.forecast = forecast;
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	@Override
	public String toString() {
		return "InvestmentViewModel [id=" + id + ", name=" + name + ", principal="
				+ principal + ", term=" + term + ", interestRate=" + interestRate
				+ ", monthlyContribution=" + monthlyContribution
				+ ", closingBalance=" + closingBalance + ", futureValue="
				+ futureValue + ", totalInterest=" + totalInterest
				+ ", totalContribution=" + totalContribution + ", events=" + events
				+ "]";
	}

}
