package com.psybergate.finance.view.model;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import javax.enterprise.context.RequestScoped;
import javax.inject.Named;

import com.psybergate.finance.domain.PropertyBondEvent;
import com.psybergate.finance.domain.Money;

@RequestScoped
@Named("propertyBondData")
public class PropertyBondDataViewModel implements Serializable {

	private static final long serialVersionUID = 5411768203181211691L;

	private Money principal;

	private Integer term;

	private Double interestRate;

	List<PropertyBondEvent> events = new ArrayList<>();

	public PropertyBondDataViewModel() {
	}

	public Money getPrincipal() {
		return principal;
	}

	public void setPrincipal(Money principal) {
		this.principal = principal;
	}

	public Integer getTerm() {
		return term;
	}

	public void setTerm(Integer term) {
		this.term = term;
	}

	public Double getInterestRate() {
		return interestRate;
	}

	public void setInterestRate(Double interestRate) {
		this.interestRate = interestRate;
	}

	public List<PropertyBondEvent> getEvents() {
		return events;
	}

	public void setEvents(List<PropertyBondEvent> events) {
		this.events = events;
	}

	@Override
	public String toString() {
		return "PropertyBondData [principal=" + principal + ", term=" + term
				+ ", interestRate=" + interestRate + ", events=" + events
				+ ", identity=" + System.identityHashCode(this) + "]";

	}

}