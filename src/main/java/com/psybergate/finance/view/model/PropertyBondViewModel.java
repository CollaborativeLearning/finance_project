package com.psybergate.finance.view.model;

import java.io.Serializable;
import java.util.List;

import javax.enterprise.inject.spi.CDI;
import javax.faces.view.ViewScoped;
import javax.inject.Named;

import com.psybergate.finance.domain.Money;
import com.psybergate.finance.domain.PropertyBondEvent;
import com.psybergate.finance.domain.PropertyForecastEntry;

@ViewScoped
@Named
public class PropertyBondViewModel implements Serializable {

	private static final long serialVersionUID = -351474641909800194L;

	private Integer id;
	
	private String propertyBondName;

	private Money principal;

	private Integer term;

	private Double interestRate;

	private Money monthlyRepayment;

	private List<PropertyForecastEntry> forecast;

	private List<PropertyBondEvent> events;

	private Money conveyanceVatFees;

	private Money conveyanceFees;

	private Money transferDutyFees;

	private Money transferFees;

	private Money totalInterest;

	private Money totalRepayment;

	private Money closingBalance;

	public PropertyBondViewModel() {
		PropertyBondDataViewModel data = CDI.current()
				.select(PropertyBondDataViewModel.class).get();
		this.setPrincipal(data.getPrincipal());
		this.setTerm(data.getTerm());
		this.setInterestRate(data.getInterestRate());
		this.setEvents(data.getEvents());
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getPropertyBondName() {
		return propertyBondName;
	}

	public void setPropertyBondName(String propertyBondName) {
		this.propertyBondName = propertyBondName;
	}

	public Money getPrincipal() {
		return principal;
	}

	public void setPrincipal(Money principal) {
		this.principal = principal;
	}

	public Integer getTerm() {
		return term;
	}

	public void setTerm(Integer term) {
		this.term = term;
	}

	public Double getInterestRate() {
		return interestRate;
	}

	public void setInterestRate(Double interestRate) {
		this.interestRate = interestRate;
	}

	public Money getMonthlyRepayment() {
		return monthlyRepayment;
	}

	public void setMonthlyRepayment(Money monthlyRepayment) {
		this.monthlyRepayment = monthlyRepayment;
	}

	public List<PropertyForecastEntry> getForecast() {
		return forecast;
	}

	public void setForecast(List<PropertyForecastEntry> forecast) {
		this.forecast = forecast;
	}

	public List<PropertyBondEvent> getEvents() {
		return events;
	}

	public void setEvents(List<PropertyBondEvent> events) {
		this.events = events;
	}

	public Money getConveyanceVatFees() {
		return conveyanceVatFees;
	}

	public void setConveyanceVatFees(Money conveyanceVatFees) {
		this.conveyanceVatFees = conveyanceVatFees;
	}

	public Money getTransferDutyFees() {
		return transferDutyFees;
	}

	public void setTransferDutyFees(Money transferDutyFees) {
		this.transferDutyFees = transferDutyFees;
	}

	public Money getConveyanceFees() {
		return conveyanceFees;
	}

	public void setConveyanceFees(Money conveyanceFees) {
		this.conveyanceFees = conveyanceFees;
	}

	public Money getTransferFees() {
		return transferFees;
	}

	public void setTransferFees(Money transferFees) {
		this.transferFees = transferFees;
	}

	public Money getTotalInterest() {
		return totalInterest;
	}

	public void setTotalInterest(Money totalInterest) {
		this.totalInterest = totalInterest;
	}

	public Money getTotalRepayment() {
		return totalRepayment;
	}

	public void setTotalRepayment(Money totalRepayment) {
		this.totalRepayment = totalRepayment;
	}

	public Money getClosingBalance() {
		return closingBalance;
	}

	public void setClosingBalance(Money closingBalance) {
		this.closingBalance = closingBalance;
	}

	@Override
	public String toString() {
		return "PropertyBondViewModel [propertyBondName=" + propertyBondName
				+ ", principal=" + principal + ", term=" + term + ", interestRate="
				+ interestRate + ", monthlyRepayment=" + monthlyRepayment
				+ /* ", forecast=" + forecast + */ ", events=" + events
				+ ", conveyanceVatFees=" + conveyanceVatFees + ", conveyanceFees="
				+ conveyanceFees + ", transferDutyFees=" + transferDutyFees
				+ ", transferFees=" + transferFees + ", totalInterest="
				+ totalInterest + ", totalRepayment=" + totalRepayment
				+ ", closingBalance=" + closingBalance + "]";
	}

}
