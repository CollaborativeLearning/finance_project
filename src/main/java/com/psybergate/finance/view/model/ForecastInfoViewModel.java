package com.psybergate.finance.view.model;

public interface ForecastInfoViewModel {

	public Integer getCustomerNum();

	public void setCustomerNum(Integer customerNum);

	public String getCustomerName();

	public void setCustomerName(String customerName);

	public String getForecastName();

	public void setForecastName(String forecastName);

}
