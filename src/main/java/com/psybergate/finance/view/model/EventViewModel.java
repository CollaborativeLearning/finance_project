package com.psybergate.finance.view.model;

import javax.enterprise.context.RequestScoped;
import javax.inject.Named;

import com.psybergate.finance.domain.Money;

@RequestScoped
@Named
public class EventViewModel {

	private static String[] TRANSACTION_OPTIONS = {"Withdraw", "Deposit"};

	private Integer month;

	private Double interestRate;

	private Money monthlyAmount;

	private Money amount;

	private boolean valid = true;

	private String transactionType;

	public Integer getMonth() {
		return month;
	}

	public void setMonth(Integer month) {
		this.month = month;
	}

	public Double getInterestRate() {
		return interestRate;
	}

	public void setInterestRate(Double interestRate) {
		this.interestRate = interestRate;
	}

	public Money getMonthlyAmount() {
		return monthlyAmount;
	}

	public void setMonthlyAmount(Money monthlyAmount) {
		this.monthlyAmount = monthlyAmount;
	}

	public Money getAmount() {
		return amount;
	}

	public void setAmount(Money amount) {
		this.amount = amount;
	}

	public boolean isValid() {
		return valid;
	}

	public void setValid(boolean valid) {
		this.valid = valid;
	}

	public String getTransactionType() {
		return transactionType;
	}

	public void setTransactionType(String transactionType) {
		this.transactionType = transactionType;
	}

	public String[] getTransactionOptions() {
		return TRANSACTION_OPTIONS;
	}

}
