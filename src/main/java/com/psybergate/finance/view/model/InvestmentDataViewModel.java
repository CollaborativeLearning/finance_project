package com.psybergate.finance.view.model;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import javax.enterprise.context.RequestScoped;
import javax.inject.Named;

import com.psybergate.finance.domain.Money;

@RequestScoped
@Named("investmentData")
public class InvestmentDataViewModel implements Serializable {

	private static final long serialVersionUID = 5411768203181211691L;

	private Integer id;

	private Money principal;

	private Integer term;

	private Money monthlyContribution;

	private Double interestRate;

	private String name;

	private List<EventViewModel> events;

	public InvestmentDataViewModel() {
		events = new ArrayList<>();
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public Money getPrincipal() {
		return principal;
	}

	public void setPrincipal(Money principal) {
		this.principal = principal;
	}

	public Integer getTerm() {
		return term;
	}

	public void setTerm(Integer term) {
		this.term = term;
	}

	public Money getMonthlyContribution() {
		return monthlyContribution;
	}

	public void setMonthlyContribution(Money monthlyContribution) {
		this.monthlyContribution = monthlyContribution;
	}

	public Double getInterestRate() {
		return interestRate;
	}

	public void setInterestRate(Double interestRate) {
		this.interestRate = interestRate;
	}

	public void setEvents(List<EventViewModel> events) {
		this.events = events;
	}

	public List<EventViewModel> getEvents() {
		return events;
	}

	@Override
	public String toString() {
		return "InvestmentDataViewModel [id=" + id + ", principal=" + principal
				+ ", term=" + term + ", monthlyContribution=" + monthlyContribution
				+ ", interestRate=" + interestRate + ", name=" + name + ", events="
				+ events + "]";
	}

}