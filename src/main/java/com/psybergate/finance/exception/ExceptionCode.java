package com.psybergate.finance.exception;

public enum ExceptionCode {
	InvalidPrincipal, InvalidInterestRate, InvalidTerm, InvalidMonth, InvalidOpeningBalance
}
