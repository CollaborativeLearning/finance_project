package com.psybergate.finance.exception;

import java.util.Map;

public class ValidationException extends ApplicationException {

	private static final long serialVersionUID = -8071034414730448706L;

	private Map<String, String> errorMessages;

	public ValidationException(Map<String, String> errorMessages) {
		this.errorMessages = errorMessages;
	}

	public Map<String, String> getErrorMessages() {
		return errorMessages;
	}

	public void setErrorMessages(Map<String, String> errorMessages) {
		this.errorMessages = errorMessages;
	}

}
