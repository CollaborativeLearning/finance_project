package com.psybergate.finance.interfaces;

import com.psybergate.finance.domain.Money;

public interface ForecastEntry extends Comparable<ForecastEntry> {

	public Money getClosingBalance();

	public Money getInterest();

	public Money getOpeningBalance();

	public Double getInterestRate();

	public Money getMonthlyAmount();

	public Integer getMonth();

	public Money getAmount();

	public ForecastEntry generateNextEntry();

	public void setInterestRate(Double interestRate);

	public void setMonthlyAmount(Money monthlyAmount);

	public void setAmount(Money amount);

	public Money getTotalContribution();
}
