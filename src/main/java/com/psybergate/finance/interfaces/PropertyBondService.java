package com.psybergate.finance.interfaces;

import java.math.BigDecimal;
import java.util.List;

import com.psybergate.finance.domain.Customer;
import com.psybergate.finance.domain.PropertyBond;
import com.psybergate.finance.domain.PropertyBondEvent;
import com.psybergate.finance.view.model.PropertyBondDataViewModel;

public interface PropertyBondService {

	public PropertyBond generatePopertyBond(
			PropertyBondDataViewModel propertyBondData);

	public List<PropertyBond> getSavedPropertyBonds(
			Integer customerNum);

	public List<PropertyBondEvent> getPropertyBondEvents(Integer id);

	public void savePropertyBond(Customer customer, String propertyBondName,
			BigDecimal principal, Integer term, Double interestRate,
			List<PropertyBondEvent> events);

	public void validateEvent(PropertyBond propertyBond, PropertyBondEvent event);

	public void validatePropertyBond(PropertyBond propertyBond,
			boolean validateEvents);

}
