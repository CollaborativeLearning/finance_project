package com.psybergate.finance.interfaces;

import java.util.List;

import com.psybergate.finance.domain.PropertyBondEvent;
import com.psybergate.finance.domain.PropertyBond;
import com.psybergate.finance.exception.ValidationException;

public interface PropertyBondValidatorService {

	public void validateEvent(PropertyBond propertyBond, PropertyBondEvent event)
			throws ValidationException;

	public void validatePropertyBond(PropertyBond propertyBond,
			boolean validateEvents) throws ValidationException;

	public void validateEvents(PropertyBond propertyBond);

	public void validateEvents(PropertyBond propertyBond, List<PropertyBondEvent> events);

}
