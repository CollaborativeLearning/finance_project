package com.psybergate.finance.interfaces;

import java.util.List;

import com.psybergate.finance.domain.Investment;
import com.psybergate.finance.domain.InvestmentEvent;

public interface InvestmentResource {

	public void saveInvestment(Investment investment);

	public List<Investment> getInvestments(Integer customerId);

	public List<InvestmentEvent> getInvestmentEvents(Integer investmentId);

	public Investment getInvestment(Integer investmentId);

	public void addEvent(Integer investmentId, InvestmentEvent event);

	public void updateInvestment(Investment investment);

}
