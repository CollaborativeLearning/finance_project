package com.psybergate.finance.interfaces;

import com.psybergate.finance.domain.InvestmentEvent;
import com.psybergate.finance.domain.Investment;
import com.psybergate.finance.exception.ValidationException;

public interface InvestmentValidatorService {

	public void validateInvestment(Investment investment, boolean validateEvents)
			throws ValidationException;

	public void validateEvents(Investment investment)
			throws ValidationException;

	public void validateEvent(Investment investment, InvestmentEvent event)
			throws ValidationException;
}
