package com.psybergate.finance.interfaces;

import com.psybergate.finance.domain.Customer;

public interface CustomerResource {

	public void addCustomer(Customer customer);

	public Customer getCustomer(Integer customerNum);

}
