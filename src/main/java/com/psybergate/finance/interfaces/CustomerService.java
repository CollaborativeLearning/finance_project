package com.psybergate.finance.interfaces;

import com.psybergate.finance.domain.Customer;

public interface CustomerService {

	public void addCustomer(Customer customer);

	public Customer getCustomer(Integer customerNum);

}
