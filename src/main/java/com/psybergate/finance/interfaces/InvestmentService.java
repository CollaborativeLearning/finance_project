package com.psybergate.finance.interfaces;

import java.util.List;

import com.psybergate.finance.domain.Investment;
import com.psybergate.finance.domain.InvestmentEvent;

public interface InvestmentService {

	public List<Investment> getInvestments(Integer customerId);

	public List<InvestmentEvent> getInvestmentEvents(Integer id);

	public void saveInvestment(Investment investment);

	public void validateInvestment(Investment investment,
			boolean validateEvents);

	public void validateEvent(Investment investment, InvestmentEvent event);

	public void addEvent(Investment investment, InvestmentEvent event);

	public void addEvent(Integer investmentId, InvestmentEvent event);

	public void updateInvestment(Investment investment);

}
