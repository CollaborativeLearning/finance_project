package com.psybergate.finance.controller;

import java.util.Map;

import javax.enterprise.inject.spi.CDI;
import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;

public abstract class FinancialProductController {

	public void renderErrorMessages(String componentId,
			Map<String, String> errorMessages) {
		FacesContext context = FacesContext.getCurrentInstance();
		for (Map.Entry<String, String> entry : errorMessages.entrySet()) {
			FacesMessage message = new FacesMessage("", entry.getValue());
			message.setSeverity(FacesMessage.SEVERITY_ERROR);
			context.addMessage(componentId + ":" + entry.getKey(), message);
		}
		context.renderResponse();
	}

	public <T> T getCurrentCDIInstance(Class<T> class1) {
		return CDI.current().select(class1).get();
	}
}
