package com.psybergate.finance.controller;

import java.math.BigDecimal;
import java.util.List;

import javax.enterprise.context.ApplicationScoped;
import javax.inject.Inject;
import javax.inject.Named;

import com.psybergate.finance.controller.dispatcher.PageDispatcher;
import com.psybergate.finance.domain.Customer;
import com.psybergate.finance.domain.Money;
import com.psybergate.finance.domain.PropertyBond;
import com.psybergate.finance.domain.PropertyBondEvent;
import com.psybergate.finance.domain.PropertyForecastEntry;
import com.psybergate.finance.exception.ValidationException;
import com.psybergate.finance.interfaces.CustomerService;
import com.psybergate.finance.interfaces.PropertyBondService;
import com.psybergate.finance.util.PrintUtil;
import com.psybergate.finance.util.ViewModelConverter;
import com.psybergate.finance.view.model.CustomerViewModel;
import com.psybergate.finance.view.model.EventViewModel;
import com.psybergate.finance.view.model.PropertyBondDataViewModel;
import com.psybergate.finance.view.model.PropertyBondViewModel;

@ApplicationScoped
@Named
public class PropertyBondController extends FinancialProductController {

	private static final String PROPERTY_BOND_EVENT_FORM = "propertyBondEventForm";

	private static final String PROPERTY_BOND_FORM = "propertyBondForm";

	@Inject
	private PropertyBondService propertyBondService;

	@Inject
	private CustomerService customerService;

	private PageDispatcher pageDispatcher = new PageDispatcher();

	public PropertyBondController() {
	}

	public void generateForecast(PropertyBondViewModel propertyBondViewModel) {
		PropertyBond propertyBond = ViewModelConverter
				.toPropertyBond(propertyBondViewModel);
		populatePropertyBondViewModel(propertyBondViewModel, propertyBond);

	}

	private void populatePropertyBondViewModel(
			PropertyBondViewModel propertyBondViewModel,
			PropertyBond propertyBond) {
		Money monthlyRepayment = propertyBond.getMonthlyRepayment();
		List<PropertyForecastEntry> forecast = propertyBond.getForecast();
		List<PropertyBondEvent> events = propertyBond.getEvents();
		Money transferDutyFees = propertyBond.getTransferDutyFees();
		Money conveyanceVatFees = propertyBond.getConveyanceVatFees();
		Money conveyanceFees = propertyBond.getConveyanceFees();
		Money totalRepayment = propertyBond.getTotalRepayment();
		Money closingBalance = propertyBond.getClosingBalance();
		Money totalInterest = propertyBond.getTotalInterest();
		Money transferFees = propertyBond.getTransferFees();

		propertyBondViewModel.setMonthlyRepayment(monthlyRepayment);
		propertyBondViewModel.setForecast(forecast);
		propertyBondViewModel.setEvents(events);
		propertyBondViewModel.setTransferDutyFees(transferDutyFees);
		propertyBondViewModel.setConveyanceVatFees(conveyanceVatFees);
		propertyBondViewModel.setConveyanceFees(conveyanceFees);
		propertyBondViewModel.setTotalRepayment(totalRepayment);
		propertyBondViewModel.setClosingBalance(closingBalance);
		propertyBondViewModel.setTotalInterest(totalInterest);
		propertyBondViewModel.setTransferFees(transferFees);
	}

	public PropertyBondEvent getEvent(EventViewModel eventViewModel) {
		Integer month = eventViewModel.getMonth();
		Double interestRate = eventViewModel.getInterestRate();
		Money amount = eventViewModel.getAmount();
		Money monthlyAmount = eventViewModel.getMonthlyAmount();
		String transactionType = eventViewModel.getTransactionType();
		if (amount != null && transactionType != null
				&& transactionType.equalsIgnoreCase("withdraw")) {
			amount = new Money(amount.getAmount().negate());
		}
		return new PropertyBondEvent(month, interestRate, amount, monthlyAmount);
	}

	public String generateForecast(PropertyBond propertyBond) {
		return pageDispatcher.getPage("propertyBondForecast");
	}

	public void addEvent(PropertyBondViewModel propertyBondViewModel,
			EventViewModel eventViewModel) {
		PropertyBond propertyBond = ViewModelConverter
				.toPropertyBond(propertyBondViewModel);
		PropertyBondEvent event = ViewModelConverter.toEvent(eventViewModel);
		try {
			propertyBondService.validateEvent(propertyBond, event);
			propertyBondViewModel.getEvents().add(event);
		} catch (ValidationException e) {
			eventViewModel.setValid(false);
			renderErrorMessages(PROPERTY_BOND_EVENT_FORM, e.getErrorMessages());
		}
	}

	public String savePropertyBond(PropertyBondViewModel propertyBondViewModel,
			CustomerViewModel customerViewModel) {
		PrintUtil.print("saving: " + propertyBondViewModel);
		PrintUtil.print("for: " + customerViewModel);
		String propertyBondName = propertyBondViewModel.getPropertyBondName();
		String customerName = customerViewModel.getName();
		Integer customerNum = customerViewModel.getCustomerNum();
		BigDecimal principal = propertyBondViewModel.getPrincipal().getAmount();
		Integer term = propertyBondViewModel.getTerm();
		Double interestRate = propertyBondViewModel.getInterestRate();
		List<PropertyBondEvent> events = propertyBondViewModel.getEvents();

		Customer customer = new Customer(customerNum, customerName);
		customerService.addCustomer(customer);
		propertyBondService.savePropertyBond(customer, propertyBondName,
				principal, term, interestRate, events);

		return pageDispatcher.getPage("propertyBondForecast");
	}

	public List<PropertyBond> getListOfPropertyBonds(
			CustomerViewModel customerViewModel) {
		Integer customerNum = customerViewModel.getCustomerNum();
		return propertyBondService.getSavedPropertyBonds(customerNum);
	}

	public String showForecast(PropertyBondDataViewModel propertyBondData) {
		PropertyBond propertyBond = ViewModelConverter
				.toPropertyBond(propertyBondData);
		try {
			propertyBondService.validatePropertyBond(propertyBond, false);
			return pageDispatcher.getPage("propertyBondForecast");
		} catch (ValidationException e) {
			PrintUtil.print("error messages: " + e.getErrorMessages());
			renderErrorMessages(PROPERTY_BOND_FORM, e.getErrorMessages());
			return null;
		}
	}

}