package com.psybergate.finance.controller;

import java.math.BigDecimal;
import java.math.RoundingMode;

import javax.enterprise.context.ApplicationScoped;
import javax.enterprise.inject.spi.CDI;
import javax.inject.Inject;
import javax.inject.Named;

import com.psybergate.finance.controller.dispatcher.PageDispatcher;
import com.psybergate.finance.domain.Customer;
import com.psybergate.finance.domain.Investment;
import com.psybergate.finance.domain.InvestmentEvent;
import com.psybergate.finance.domain.Money;
import com.psybergate.finance.exception.ValidationException;
import com.psybergate.finance.interfaces.CustomerService;
import com.psybergate.finance.interfaces.InvestmentService;
import com.psybergate.finance.util.PrintUtil;
import com.psybergate.finance.util.ViewModelConverter;
import com.psybergate.finance.view.model.CustomerViewModel;
import com.psybergate.finance.view.model.EventViewModel;
import com.psybergate.finance.view.model.InvestmentDataViewModel;
import com.psybergate.finance.view.model.InvestmentViewModel;
import com.psybergate.web.view.forecast.investment.InvestmentLineVisualisation;

@ApplicationScoped
@Named("investmentController")
public class InvestmentController extends FinancialProductController {

	private static final String EVENT_FORM = "tabView:investmentEventForm";

	private static final String INVESTMENT_FORM = "investmentForm";

	@Inject
	private InvestmentService investmentService;

	@Inject
	private CustomerService customerService;

	private PageDispatcher pageDispatcher = new PageDispatcher();

	public InvestmentController() {
	}

	public void generateForecast(InvestmentViewModel investmentViewModel) {
		Investment investment = ViewModelConverter
				.toInvestment(investmentViewModel);
		try {
			investmentService.validateInvestment(investment, false);
		} catch (ValidationException e) {
			PrintUtil.print("error messages: " + e.getErrorMessages());
			renderErrorMessages(INVESTMENT_FORM, e.getErrorMessages());
			return;
		}
		investmentViewModel
				.setMonthlyContribution(investment.getMonthlyContribution());
		investmentViewModel
				.setTotalContribution(investment.getTotalContribution());
		investmentViewModel.setTotalInterest(investment.getTotalInterest());
		investmentViewModel.setClosingBalance(investment.getClosingBalance());
		investmentViewModel.setFutureValue(investment.getFutureValue());
		investmentViewModel.setForecast(investment.getForecast());
	}

	public String showForecast(InvestmentDataViewModel investmentData) {
		Investment investment = ViewModelConverter.toInvestment(investmentData);
		try {
			investmentService.validateInvestment(investment, false);
			return pageDispatcher.getPage("investmentForecast");
		} catch (ValidationException e) {
			renderErrorMessages(INVESTMENT_FORM, e.getErrorMessages());
			return null;
		}
	}

	public void addEvent(InvestmentViewModel investmentViewModel,
			EventViewModel eventViewModel) {
		PrintUtil.print("investmentViewModel: " + investmentViewModel);
		Investment investment = ViewModelConverter
				.toInvestment(investmentViewModel);
		PrintUtil.print("investment: " + investment);
		InvestmentEvent event = ViewModelConverter
				.toInvestmentEvent(eventViewModel);
		try {
			if (event.additionalAmountChanged() || event.interestRateChanged()
					|| event.monthlyAmountChanged()) {
				investmentService.validateEvent(investment, event);
				if (investment.getId() != null) {
					investmentService.addEvent(investment, event);
				}
				investmentViewModel.getEvents().add(eventViewModel);
			}
		} catch (ValidationException e) {
			eventViewModel.setValid(false);
			renderErrorMessages(EVENT_FORM, e.getErrorMessages());
		}
	}

	public Investment getInvestment(InvestmentDataViewModel investmentData) {
		return ViewModelConverter.toInvestment(investmentData);
	}

	public Investment getInvestment(InvestmentViewModel investmentViewModel) {
		return ViewModelConverter.toInvestment(investmentViewModel);
	}

	public boolean isEmpty(InvestmentDataViewModel investmentData) {
		return (investmentData.getTerm() == null
				&& investmentData.getPrincipal() == null
				&& investmentData.getMonthlyContribution() == null
				&& investmentData.getInterestRate() == null
				&& investmentData.getEvents() == null);
	}

	public String saveInvestment(InvestmentViewModel investmentViewModel,
			CustomerViewModel customerViewModel) {
		Customer customer = customerService
				.getCustomer(customerViewModel.getCustomerNum());

		if (customer == null) {
			customer = new Customer(customerViewModel.getCustomerNum(),
					customerViewModel.getName());
			customerService.addCustomer(customer);
			PrintUtil.print("saved: " + customer);
		}
		else {
			PrintUtil.print("customer already exists: " + customer);
		}
		Investment investment = ViewModelConverter
				.toInvestment(investmentViewModel);
		investment.setCustomerId(customer.getId());
		investmentService.saveInvestment(investment);
		investmentViewModel.setId(investment.getId());
		return null;
	}

	public String someMethod() {
		try {
			InvestmentViewModel investmentViewModel = getCurrentCDIInstance(
					InvestmentViewModel.class);
			InvestmentLineVisualisation lineVisual = getCurrentCDIInstance(
					InvestmentLineVisualisation.class);
			BigDecimal closingBalance = investmentViewModel.getClosingBalance()
					.getAmount();
			BigDecimal principalAmount = investmentViewModel.getPrincipal()
					.getAmount();
			BigDecimal principalTermAsBigDecimal = lineVisual
					.getPrincipalTermAsBigDecimal(investmentViewModel);
			String result = "".concat(closingBalance.subtract(principalAmount)
					.divide(principalTermAsBigDecimal, 2, RoundingMode.HALF_UP)
					.toString());
			return result;
		} catch (Exception e) {
			return "2500.00";
		}
	}

	public String someMethod2() {
		InvestmentViewModel investmentViewModel = CDI.current()
				.select(InvestmentViewModel.class).get();
		Money principal = investmentViewModel.getPrincipal();
		Money closingBalance = investmentViewModel.getClosingBalance();
		try {
			return closingBalance.subtract(principal).getAmount().toString();
		} catch (Exception e) {
			return "2500.00";
		}
	}
}
