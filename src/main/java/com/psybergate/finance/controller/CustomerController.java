package com.psybergate.finance.controller;

import javax.enterprise.context.ApplicationScoped;
import javax.inject.Inject;

import com.psybergate.finance.domain.Customer;
import com.psybergate.finance.interfaces.CustomerService;

@ApplicationScoped
public class CustomerController {

	@Inject
	private CustomerService customerService;

	public void addCustomer(Customer customer) {
		customerService.addCustomer(customer);
	}

}
