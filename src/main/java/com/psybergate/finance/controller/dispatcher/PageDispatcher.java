package com.psybergate.finance.controller.dispatcher;

import java.io.IOException;
import java.io.InputStream;
import java.util.HashMap;
import java.util.Map;
import java.util.Properties;

import javax.enterprise.context.ApplicationScoped;
import javax.inject.Named;

@ApplicationScoped
@Named("dispatcher")
public class PageDispatcher {

	public static final String WEBPAGE_MAPPINGS = "mappings/webpagemappings.properties";

	/**
	 * Format: {pageType specified in xhtml: the next xhtml file to be rendered}
	 */
	private Map<String, String> webPages;

	public PageDispatcher() {
		super();
		loadWebPageMappings();
	}

	public String getPage(String pageName) {
		return webPages.get(pageName.toLowerCase());
	}

	private void loadWebPageMappings() {
		Properties props = loadProperties(WEBPAGE_MAPPINGS);

		Map<String, String> webPages = new HashMap<String, String>();

		for (Map.Entry<Object, Object> webPageEntry : props.entrySet()) {
			String pageType = ((String) webPageEntry.getKey()).toLowerCase();
			String pageSourceFile = (String) webPageEntry.getValue();
			webPages.put(pageType, pageSourceFile);
		}

		this.webPages = webPages;
	}

	private Properties loadProperties(String propertiesFile) {
		try {
			Properties props = new Properties();
			InputStream propsFileStream = Thread.currentThread().getContextClassLoader().getResourceAsStream(propertiesFile);
			props.load(propsFileStream);
			propsFileStream.close();
			return props;
		}
		catch (IOException e) {
			throw new RuntimeException(e);
		}
	}

}
