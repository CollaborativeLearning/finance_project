package com.psybergate.finance.domain;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

import javax.persistence.Convert;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToMany;
import javax.persistence.Transient;

import com.psybergate.finance.util.MoneyToNumericConverter;
import com.psybergate.finance.util.PropertyBondCalculatorUtil;

@Entity
public class PropertyBond {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Integer id;

	private Integer customerId;

	private String name;

	private Integer term;

	private Double interestRate;

	@Convert(converter = MoneyToNumericConverter.class)
	private Money principal;

	@Transient
	private List<PropertyForecastEntry> forecast = new ArrayList<>();

	@OneToMany
	@JoinColumn(name = "propertybondid")
	private List<PropertyBondEvent> events = new ArrayList<>();

	@Transient
	private boolean paidUp;

	public PropertyBond() {
	}

	public boolean isPaidUp() {
		return paidUp;
	}

	private void setPaidUp(boolean paidUp) {
		this.paidUp = paidUp;
	}

	public PropertyBond(Integer id, String name, Integer term,
			Double interestRate, Money principal, List<PropertyBondEvent> events) {
		this.id = id;
		this.name = name;
		this.principal = principal;
		this.term = term;
		this.interestRate = interestRate;
		this.events = events;
		generateEntries();
	}

	public PropertyBond(Integer term, Double interestRate, Money principal,
			List<PropertyBondEvent> events) {
		this(null, null, term, interestRate, principal, events);
	}

	public Money getMonthlyRepayment() {
		return PropertyBondCalculatorUtil.getMonthlyRepayment(interestRate, term,
				principal);
	}

	public Money getTransferFees() {
		return getConveyanceFees().add(getTransferDutyFees())
				.add(getConveyanceVatFees());
	}

	public Money getConveyanceFees() {
		return PropertyBondCalculatorUtil.getConveyanceFees(principal);
	}

	public Money getTransferDutyFees() {
		return PropertyBondCalculatorUtil.getTransferDutyFees(principal);
	}

	public Money getConveyanceVatFees() {
		return PropertyBondCalculatorUtil.getConveyanceVatFees(principal);
	}

	public List<PropertyForecastEntry> getForecast() {
		return forecast;
	}

	public Integer getTerm() {
		return term;
	}

	public void setTerm(Integer term) {
		this.term = term;
	}

	public Double getInterestRate() {
		return interestRate;
	}

	public void setInterestRate(Double interestRate) {
		this.interestRate = interestRate;
	}

	public Money getPrincipal() {
		return principal;
	}

	public void setPrincipal(Money openingBalance) {
		this.principal = openingBalance;
	}

	public Integer getCustomerId() {
		return customerId;
	}

	public void setCustomerId(Integer customerId) {
		this.customerId = customerId;
	}

	public List<PropertyBondEvent> getEvents() {
		return events;
	}

	public void setEvents(List<PropertyBondEvent> events) {
		this.events = events;
		generateEntries();
	}

	private void setForecast(List<PropertyForecastEntry> forecast) {
		this.forecast = forecast;
	}

	public Money getClosingBalance() {
		return forecast.get(forecast.size() - 1).getClosingBalance();
	}

	public Money getTotalInterest() {
		Money totalRepayment = getTotalRepayment();
		return totalRepayment.subtract(principal);
	}

	public Money getTotalRepayment() {
		Money totalRepayment = new Money();
		for (PropertyForecastEntry forecastEntry : forecast) {
			Money totalRepaymentForMonth = forecastEntry.getMonthlyAmount()
					.add(forecastEntry.getAmount());
			totalRepayment = totalRepayment.add(totalRepaymentForMonth);
		}
		return totalRepayment;
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	private void addPropertyForecastEntry(PropertyForecastEntry entry) {
		forecast.add(entry);
	}

	public void generateEntries() {
		setForecast(new ArrayList<>());
		if (term == null || term < 1 || principal == null
				|| interestRate == null) {
			return;
		}
		PropertyForecastEntry entry = new PropertyForecastEntry(1, interestRate,
				principal, getMonthlyRepayment(), new Money());
		applyEvents(entry);
		addPropertyForecastEntry(entry);
		for (Integer month = 2; month <= getTerm() && !paidUp; month++) {
			PropertyForecastEntry prevEntry = getForecast().get(month - 2);
			entry = prevEntry.generateNextEntry();
			applyEvents(entry);
			paidUp = entry.getClosingBalance().getAmount().doubleValue() <= 0;
			addPropertyForecastEntry(entry);
		}
		clearBalance(entry);
	}

	private void clearBalance(PropertyForecastEntry entry) {
		Money correction = entry.getMonthlyAmount()
				.add(entry.getClosingBalance());
		entry.setMonthlyAmount(correction);
		setPaidUp(true);
	}

	public Integer getMonthPaidUp() {
		if (isPaidUp()) {
			return forecast.size();
		}
		return null;
	}

	private void applyEvents(PropertyForecastEntry entry) {
		PropertyBondEvent event = getEventForMonth(entry.getMonth());
		if (event != null) {
			Money newMonthlyRepayment = PropertyBondCalculatorUtil
					.getNewMonthlyRepayment(entry, event,
							term - entry.getMonth() + 1);
			Money currentMonthlyRepayment = entry.getMonthlyAmount();
			if (event.monthlyAmountChanged()) {
				currentMonthlyRepayment = event.getMonthlyAmount();
			}
			if (currentMonthlyRepayment.compareTo(newMonthlyRepayment) < 0) {
				event.setMonthlyAmount(newMonthlyRepayment);
			}
			entry.applyEvent(event);
		}
	}

	private PropertyBondEvent getEventForMonth(Integer month) {
		for (PropertyBondEvent event : events) {
			if (event.getMonth() == month) return event;
		}
		return null;
	}

	public void addEvent(PropertyBondEvent event) {
		events.add(event);
		generateEntries();
	}

	public Integer getMonthSettled() {
		Money closingBalance = getClosingBalance();
		if (closingBalance != null) {
			if (closingBalance.getAmount().compareTo(BigDecimal.ZERO) <= 0) {
				return forecast.size();
			}
		}
		return null;
	}

}
