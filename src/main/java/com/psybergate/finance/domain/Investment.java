package com.psybergate.finance.domain;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Convert;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToMany;
import javax.persistence.Transient;

import com.psybergate.finance.util.MoneyToNumericConverter;

@Entity
public class Investment {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Integer id;

	private Integer customerId;

	private String name;

	@Convert(converter = MoneyToNumericConverter.class)
	private Money principal;

	private Double interestRate;

	private Integer term;

	@Convert(converter = MoneyToNumericConverter.class)
	private Money monthlyContribution;

	@Transient
	private List<InvestmentForecastEntry> forecast;

	@OneToMany(cascade = CascadeType.ALL, orphanRemoval = true)
	@JoinColumn(name = "investmentid")
	private List<InvestmentEvent> events;

	public Investment() {
	}

	public Investment(Money principal, Double interestRate, Integer term,
			Money monthlyContribution, List<InvestmentEvent> events) {
		this(null, null, principal, interestRate, term, monthlyContribution,
				events);
	}

	public Investment(Integer customerId, String investmentName, Money principal,
			Double interestRate, Integer term, Money monthlyContribution,
			List<InvestmentEvent> events) {
		if (monthlyContribution == null) {
			monthlyContribution = new Money();
		}
		this.principal = principal;
		this.interestRate = interestRate;
		this.term = term;
		this.monthlyContribution = monthlyContribution;
		this.events = events;
		generateEntries();
	}

	private void addInvestmentForecastEntry(InvestmentForecastEntry entry) {
		forecast.add(entry);
	}

	public Money getFutureValue() {
		if (forecast == null || forecast.isEmpty()) {
			return new Money();
		}
		InvestmentForecastEntry lastInvestmentForecastEntry = forecast
				.get(forecast.size() - 1);
		return lastInvestmentForecastEntry.getClosingBalance();
	}

	public Money getTotalInterest() {
		return getFutureValue().subtract(getTotalContribution());
	}

	public List<InvestmentForecastEntry> getForecast() {
		return forecast;
	}

	public Money getPrincipal() {
		return principal;
	}

	public void generateEntries() {
		setForecast(new ArrayList<>());
		if (term == null || term < 1 || interestRate == null || principal == null)
			return;
		InvestmentForecastEntry firstEntry = new InvestmentForecastEntry(1,
				interestRate, principal, monthlyContribution, new Money());
		handleEvents(firstEntry);
		addInvestmentForecastEntry(firstEntry);
		for (Integer month = 2; month <= getTerm(); month++) {
			InvestmentForecastEntry prevEntry = getForecast().get(month - 2);
			InvestmentForecastEntry entry = prevEntry.generateNextEntry();
			handleEvents(entry);
			addInvestmentForecastEntry(entry);
		}
	}

	private void handleEvents(InvestmentForecastEntry entry) {
		Integer month = entry.getMonth();
		InvestmentEvent event = getEvent(month);
		entry.applyEvent(event);
	}

	public InvestmentEvent getEvent(Integer month) {
		if (month == null) {
			return null;
		}
		for (InvestmentEvent event : getEvents()) {
			if (month == event.getMonth()) {
				return event;
			}
		}
		return null;
	}

	public void setPrincipal(Money principal) {
		this.principal = principal;
	}

	public Double getInterestRate() {
		return interestRate;
	}

	public void setInterestRate(Double interestRate) {
		this.interestRate = interestRate;
	}

	public Integer getTerm() {
		return term;
	}

	public void setTerm(Integer term) {
		this.term = term;
	}

	public Money getMonthlyContribution() {
		return monthlyContribution;
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public void setMonthlyContribution(Money monthlyContribution) {
		if (monthlyContribution == null) {
			monthlyContribution = new Money();
		}
		this.monthlyContribution = monthlyContribution;
	}

	private void setForecast(List<InvestmentForecastEntry> forecast) {
		this.forecast = forecast;
	}

	public List<InvestmentEvent> getEvents() {
		return events;
	}

	public void setEvents(List<InvestmentEvent> events) {
		this.events = events;
		generateEntries();
	}

	public boolean hasEvents() {
		return events != null && !events.isEmpty();
	}

	public Integer getCustomerId() {
		return customerId;
	}

	public void setCustomerId(Integer customerId) {
		this.customerId = customerId;
	}

	public Money getClosingBalance() {
		return getFutureValue();
	}

	public Money getTotalContribution() {
		Money totalCapital = principal;
		for (InvestmentForecastEntry entry : forecast) {
			totalCapital = totalCapital.add(entry.getTotalContribution());
		}
		return totalCapital;
	}

	@Override
	public String toString() {
		return "Investment [id=" + id + ", customerId=" + customerId + ", name="
				+ name + ", principal=" + principal + ", interestRate="
				+ interestRate + ", term=" + term + ", monthlyContribution="
				+ monthlyContribution + ", events=" + events + "]";
	}

}