package com.psybergate.finance.domain;

import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToMany;

@Entity
public class Customer {

	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Id
	private Integer id;

	private Integer customerNum;

	private String name;

	@OneToMany(cascade = CascadeType.ALL, orphanRemoval = true)
	@JoinColumn(name = "customerid")
	private List<Investment> investments;

	@OneToMany(cascade = CascadeType.ALL, orphanRemoval = true)
	@JoinColumn(name = "customerid")
	private List<PropertyBond> propertyBonds;

	public Customer() {
	}

	public Customer(Integer customerNum, String name) {
		this.customerNum = customerNum;
		this.name = name;
	}

	public Integer getCustomerNum() {
		return customerNum;
	}

	public void setCustomerNum(Integer customerNum) {
		this.customerNum = customerNum;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public List<Investment> getInvestments() {
		return investments;
	}

	public void setInvestments(List<Investment> investments) {
		this.investments = investments;
	}

	public List<PropertyBond> getPropertyBonds() {
		return propertyBonds;
	}

	public void setPropertyBonds(List<PropertyBond> propertyBonds) {
		this.propertyBonds = propertyBonds;
	}

	@Override
	public String toString() {
		return "Customer [id=" + id + ", customerNum=" + customerNum + ", name="
				+ name + "]";
	}

}
