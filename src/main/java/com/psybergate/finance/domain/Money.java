package com.psybergate.finance.domain;

import java.math.BigDecimal;

public class Money implements Comparable<Money> {

	private BigDecimal amount;

	public Money() {
		this(BigDecimal.ZERO.setScale(2, BigDecimal.ROUND_CEILING));
	}

	public Money(BigDecimal amount) {
		if (amount == null) {
			amount = BigDecimal.ZERO;
		}
		this.amount = amount.setScale(2, BigDecimal.ROUND_CEILING);
	}

	public Money(double amount) {
		this(BigDecimal.valueOf(amount));
	}

	public Money(String amount) {
		this(new BigDecimal(amount));
	}

	public Money add(Money money) {
		if (money == null) return this;
		return add(money.getAmount());
	}

	public Money add(double money) {
		return add(BigDecimal.valueOf(money));
	}

	private Money add(BigDecimal valueOf) {
		return new Money(
				amount.add(valueOf).setScale(2, BigDecimal.ROUND_CEILING));
	}

	public Money subtract(Money money) {
		if (money == null) return this;
		return new Money(this.getAmount().subtract(money.getAmount()).setScale(2,
				BigDecimal.ROUND_CEILING));
	}

	public Money subtract(double money) {
		return new Money(amount.subtract(BigDecimal.valueOf(money)));
	}

	public Money multiply(double multiplicand) {
		return new Money(amount.multiply(BigDecimal.valueOf(multiplicand)));
	}

	public BigDecimal getAmount() {
		return amount;
	}

	public Money divide(double divisor) {
		return new Money(amount.divide(BigDecimal.valueOf(divisor)));
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj) return true;
		if (obj == null) return false;
		if (getClass() != obj.getClass()) return false;
		Money other = (Money) obj;
		if (amount == null) {
			if (other.amount != null) return false;
		}
		else if (!amount.equals(other.amount)) {
			return false;
		}
		return true;
	}

	@Override
	public int hashCode() {
		return super.hashCode();
	}

	@Override
	public int compareTo(Money money) {
		return this.getAmount().compareTo(money.getAmount());
	}

	@Override
	public String toString() {
		return String.format("%s", amount);
	}

}