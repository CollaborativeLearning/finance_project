package com.psybergate.finance.domain;

import java.math.BigDecimal;

import com.psybergate.finance.exception.ApplicationException;
import com.psybergate.finance.exception.ExceptionCode;
import com.psybergate.finance.interfaces.ForecastEntry;

public class InvestmentForecastEntry implements ForecastEntry {

	private static final Integer MINIMUM_INTEREST_RATE = 0;

	private static final Integer MAXIMUM_INTEREST_RATE = 100;

	private static final Integer MINIMUM_MONTH = 1;

	private Integer month;

	private Double interestRate;

	private Money openingBalance;

	private Money monthlyAmount;

	private Money amount;

	public InvestmentForecastEntry() {
	}

	public InvestmentForecastEntry(Integer month, Double interestRate,
			Money openingBalance, Money monthlyAmount, Money amount) {
		if (monthlyAmount == null) {
			monthlyAmount = new Money();
		}
		if (amount == null) {
			amount = new Money();
		}
		this.month = month;
		this.interestRate = interestRate;
		this.openingBalance = openingBalance;
		this.monthlyAmount = monthlyAmount;
		this.amount = amount;
	}

	@Override
	public InvestmentForecastEntry generateNextEntry() {
		return new InvestmentForecastEntry(month + 1, interestRate,
				getClosingBalance(), monthlyAmount, new Money());
	}

	@Override
	public Money getInterest() {
		Double interestRate = getActualInterestRate() / 12;
		Money interest = (openingBalance.add(getTotalContribution())
				.multiply(interestRate));
		return interest;
	}

	@Override
	public Money getTotalContribution() {
		return monthlyAmount.add(amount);
	}

	private Double getActualInterestRate() {
		return interestRate / 100;
	}

	@Override
	public Money getClosingBalance() {
		Money closingBalance = openingBalance.add(getInterest())
				.add(getTotalContribution());
		return closingBalance;
	}

	@Override
	public int compareTo(ForecastEntry o) {
		return this.getMonth() - o.getMonth();
	}

	@Override
	public Integer getMonth() {
		return month;
	}

	public void setMonth(Integer month) {
		if (month < MINIMUM_MONTH) {
			throw new ApplicationException(ExceptionCode.InvalidMonth,
					"Invalid month in forecast entry");
		}
		this.month = month;
	}

	@Override
	public Double getInterestRate() {
		return interestRate;
	}

	@Override
	public void setInterestRate(Double interestRate) {
		if (interestRate < MINIMUM_INTEREST_RATE
				|| interestRate > MAXIMUM_INTEREST_RATE) {
			throw new ApplicationException(ExceptionCode.InvalidInterestRate,
					"Invalid interest rate in forecast entry");
		}
		this.interestRate = interestRate;
	}

	@Override
	public Money getOpeningBalance() {
		return openingBalance;
	}

	public void setOpeningBalance(Money openingBalance) {
		if (openingBalance == null
				|| (openingBalance.compareTo(new Money(BigDecimal.ZERO)) <= 0)) {
			throw new ApplicationException(ExceptionCode.InvalidOpeningBalance,
					"Invalid opening balance in forecast entry");
		}
		this.openingBalance = openingBalance;
	}

	@Override
	public Money getMonthlyAmount() {
		return monthlyAmount;
	}

	@Override
	public void setMonthlyAmount(Money monthlyAmount) {
		if (monthlyAmount == null) {
			monthlyAmount = new Money();
		}
		this.monthlyAmount = monthlyAmount;
	}

	@Override
	public Money getAmount() {
		return amount;
	}

	@Override
	public void setAmount(Money amount) {
		if (amount == null) {
			amount = new Money();
		}
		this.amount = amount;
	}

	@Override
	public String toString() {
		return "ForecastEntry [month=" + month + ", interestRate=" + interestRate
				+ ", openingBalance=" + openingBalance + ", actual interest rate="
				+ getActualInterestRate() + ", closing balance="
				+ getClosingBalance() + "]";
	}

	public void applyEvent(InvestmentEvent event) {
		if (event == null) return;
		Integer month = getMonth();
		if (event.getMonth() == month) {
			if (event.interestRateChanged()) {
				setInterestRate(event.getInterestRate());
			}
			if (event.monthlyAmountChanged()) {
				setMonthlyAmount(event.getMonthlyContribution());
			}
			if (event.additionalAmountChanged()) {
				setAmount(event.getAmount());
			}
		}
	}

}
