package com.psybergate.finance.domain;

import java.math.BigDecimal;

import com.psybergate.finance.exception.ApplicationException;
import com.psybergate.finance.exception.ExceptionCode;
import com.psybergate.finance.interfaces.ForecastEntry;

public class PropertyForecastEntry implements ForecastEntry {

	private static final Integer MINIMUM_INTEREST_RATE = 1;

	private static final Integer MAXIMUM_INTEREST_RATE = 100;

	private static final Integer MINIMUM_MONTH = 1;

	private Integer month;

	private Double interestRate;

	private Money openingBalance;

	private Money monthlyAmount;

	private Money amount;

	public PropertyForecastEntry() {
	}

	public PropertyForecastEntry(Integer month, Double interestRate,
			Money openingBalance, Money monthlyAmount, Money amount) {
		this.month = month;
		this.interestRate = interestRate;
		this.openingBalance = openingBalance;
		this.monthlyAmount = monthlyAmount;
		this.amount = amount;
	}

	@Override
	public PropertyForecastEntry generateNextEntry() {
		return new PropertyForecastEntry(month + 1, interestRate,
				getClosingBalance(), monthlyAmount, new Money());
	}

	@Override
	public Money getInterest() {
		Double interestRate = getActualInterestRate() / 12;
		Money interest = (openingBalance.multiply(interestRate));
		return interest;
	}

	private Money getTotalAmount() {
		return monthlyAmount.add(amount);
	}

	private Double getActualInterestRate() {
		return interestRate / 100;
	}

	@Override
	public Money getClosingBalance() {
		Money closingBalance = openingBalance.add(getInterest())
				.subtract(getTotalAmount());
		return closingBalance;
	}

	public void clearClosingBalance() {
		setMonthlyAmount(openingBalance.add(getInterest()));
	}

	@Override
	public int compareTo(ForecastEntry o) {
		return this.getMonth() - o.getMonth();
	}

	public Integer getMonth() {
		return month;
	}

	public void setMonth(Integer month) {
		if (month < MINIMUM_MONTH) {
			throw new ApplicationException(ExceptionCode.InvalidMonth,
					"Invalid month in forecast entry");
		}
		this.month = month;
	}

	@Override
	public Double getInterestRate() {
		return interestRate;
	}

	@Override
	public void setInterestRate(Double interestRate) {
		if (interestRate < MINIMUM_INTEREST_RATE
				|| interestRate > MAXIMUM_INTEREST_RATE) {
			throw new ApplicationException(ExceptionCode.InvalidInterestRate,
					"Invalid interest rate in forecast entry: " + interestRate);
		}
		this.interestRate = interestRate;
	}

	@Override
	public Money getOpeningBalance() {
		return openingBalance;
	}

	public void setOpeningBalance(Money openingBalance) {
		if (openingBalance == null
				|| (openingBalance.compareTo(new Money(BigDecimal.ZERO)) <= 0)) {
			throw new ApplicationException(ExceptionCode.InvalidOpeningBalance,
					"Invalid opening balance in forecast entry");
		}
		this.openingBalance = openingBalance;
	}

	@Override
	public Money getMonthlyAmount() {
		return monthlyAmount;
	}

	@Override
	public void setMonthlyAmount(Money monthlyAmount) {
		this.monthlyAmount = monthlyAmount;
	}

	@Override
	public Money getAmount() {
		return amount;
	}

	@Override
	public void setAmount(Money amount) {
		this.amount = amount;
	}

	public void applyEvent(PropertyBondEvent event) {
		if (event == null) return;
		Integer month = getMonth();
		if (event.getMonth() == month) {
			if (event.interestRateChanged()) {
				setInterestRate(event.getInterestRate());
			}
			if (event.monthlyAmountChanged()) {
				setMonthlyAmount(event.getMonthlyAmount());
			}
			if (event.additionalAmountChanged()) {
				setAmount(event.getAmount());
			}
		}
	}

	@Override
	public String toString() {
		return "ForecastEntry [month=" + month + ", interestRate=" + interestRate
				+ ", openingBalance=" + openingBalance + ", actual interest rate="
				+ getActualInterestRate() + ", closing balance="
				+ getClosingBalance() + "]";
	}

	@Override
	public Money getTotalContribution() {
		return getMonthlyAmount().add(getAmount());
	}
}
