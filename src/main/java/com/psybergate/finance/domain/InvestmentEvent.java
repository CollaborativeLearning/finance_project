package com.psybergate.finance.domain;

import javax.persistence.Convert;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

import com.psybergate.finance.util.MoneyToNumericConverter;

@Entity(name = "investmentevent")
public class InvestmentEvent {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Integer id;

	private Integer investmentId;

	private Integer month;

	private Double interestRate;

	@Convert(converter = MoneyToNumericConverter.class)
	private Money amount;

	@Convert(converter = MoneyToNumericConverter.class)
	private Money monthlyContribution;

	public InvestmentEvent() {
	}

	public InvestmentEvent(Integer month, Double interestRate, Money amount,
			Money monthlyAmount) {
		this.month = month;
		this.interestRate = interestRate;
		this.amount = amount;
		this.monthlyContribution = monthlyAmount;
	}

	public InvestmentEvent(Integer month, Double interestRate, Money amount,
			Money monthlyAmount, String instrumentType) {
		super();
		this.month = month;
		this.interestRate = interestRate;
		this.amount = amount;
		this.monthlyContribution = monthlyAmount;
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public Integer getMonth() {
		return month;
	}

	public void setMonth(Integer month) {
		this.month = month;
	}

	public Double getInterestRate() {
		return interestRate;
	}

	public void setInterestRate(Double interestRate) {
		this.interestRate = interestRate;
	}

	public Money getAmount() {
		return amount;
	}

	public void setAmount(Money amount) {
		this.amount = amount;
	}

	public Money getMonthlyContribution() {
		return monthlyContribution;
	}

	public void setMonthlyAmount(Money monthlyAmount) {
		this.monthlyContribution = monthlyAmount;
	}

	public Integer getInvestmentId() {
		return investmentId;
	}

	public void setInvestmentId(Integer investmentId) {
		this.investmentId = investmentId;
	}

	public boolean interestRateChanged() {
		return interestRate != null;
	}

	public boolean monthlyAmountChanged() {
		return monthlyContribution != null;
	}

	public boolean additionalAmountChanged() {
		return amount != null;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj) return true;
		if (obj == null || obj.getClass() != this.getClass()) return false;
		InvestmentEvent that = (InvestmentEvent) obj;
		return equal(month, that.month)
				&& monthlyContribution.equals(that.monthlyContribution)
				&& equal(interestRate, that.interestRate)
				&& (amount != null && amount.equals(that.amount)
						|| (amount == null && that.amount == null));
	}

	@Override
	public String toString() {
		return "Event [id=" + id + ", month=" + month + ", interestRate="
				+ interestRate + ", amount=" + amount + ", monthlyAmount="
				+ monthlyContribution + "]";
	}

	private static boolean equal(Number decimal, Number decimal2) {
		if (decimal == null) {
			return decimal2 == null;
		}
		return decimal.equals(decimal2);
	}

}
