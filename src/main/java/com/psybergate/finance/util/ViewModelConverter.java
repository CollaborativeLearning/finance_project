package com.psybergate.finance.util;

import java.util.ArrayList;
import java.util.List;

import com.psybergate.finance.domain.Customer;
import com.psybergate.finance.domain.Investment;
import com.psybergate.finance.domain.InvestmentEvent;
import com.psybergate.finance.domain.Money;
import com.psybergate.finance.domain.PropertyBond;
import com.psybergate.finance.domain.PropertyBondEvent;
import com.psybergate.finance.view.model.CustomerViewModel;
import com.psybergate.finance.view.model.EventViewModel;
import com.psybergate.finance.view.model.InvestmentDataViewModel;
import com.psybergate.finance.view.model.InvestmentViewModel;
import com.psybergate.finance.view.model.PropertyBondDataViewModel;
import com.psybergate.finance.view.model.PropertyBondViewModel;

public class ViewModelConverter {

	public static List<EventViewModel> toEventViewModel(
			List<PropertyBondEvent> events) {
		List<EventViewModel> eventsModel = new ArrayList<>();
		for (PropertyBondEvent entry : events) {
			eventsModel.add(toEventViewModel(entry));
		}
		return eventsModel;
	}

	public static List<EventViewModel> toInvestmentEventViewModel(
			List<InvestmentEvent> events) {
		List<EventViewModel> eventsModel = new ArrayList<>();
		for (InvestmentEvent event : events) {
			eventsModel.add(toInvestmentEventViewModel(event));
		}
		return eventsModel;
	}

	public static EventViewModel toInvestmentEventViewModel(
			InvestmentEvent event) {
		EventViewModel eventViewModel = new EventViewModel();
		eventViewModel.setAmount(event.getAmount());
		eventViewModel.setInterestRate(event.getInterestRate());
		eventViewModel.setMonth(event.getMonth());
		eventViewModel.setMonthlyAmount(event.getMonthlyContribution());
		return eventViewModel;
	}

	public static EventViewModel toEventViewModel(PropertyBondEvent event) {
		EventViewModel eventViewModel = new EventViewModel();
		eventViewModel.setAmount(event.getAmount());
		eventViewModel.setInterestRate(event.getInterestRate());
		eventViewModel.setMonth(event.getMonth());
		eventViewModel.setMonthlyAmount(event.getMonthlyAmount());
		return eventViewModel;
	}

	public static PropertyBondEvent toEvent(EventViewModel eventViewModel) {
		Integer month = eventViewModel.getMonth();
		Double interestRate = eventViewModel.getInterestRate();
		Money amount = eventViewModel.getAmount();
		Money monthlyAmount = eventViewModel.getMonthlyAmount();
		PropertyBondEvent event = new PropertyBondEvent(month, interestRate,
				amount, monthlyAmount);
		String transactionType = eventViewModel.getTransactionType();
		if (amount != null && transactionType != null
				&& transactionType.equalsIgnoreCase("withdraw")) {
			event.setAmount(new Money(amount.getAmount().negate()));
		}
		return event;
	}

	public static InvestmentEvent toInvestmentEvent(
			EventViewModel eventViewModel) {
		Integer month = eventViewModel.getMonth();
		Double interestRate = eventViewModel.getInterestRate();
		Money amount = eventViewModel.getAmount();
		Money monthlyAmount = eventViewModel.getMonthlyAmount();
		InvestmentEvent event = new InvestmentEvent(month, interestRate, amount,
				monthlyAmount);
		String transactionType = eventViewModel.getTransactionType();
		if (amount != null && transactionType != null
				&& transactionType.equalsIgnoreCase("withdraw")) {
			event.setAmount(new Money(amount.getAmount().negate()));
		}
		return event;
	}

	public static List<PropertyBondEvent> toEvent(
			List<EventViewModel> eventsViewModel) {
		List<PropertyBondEvent> events = new ArrayList<>();
		if (eventsViewModel != null) {
			for (EventViewModel entry : eventsViewModel) {
				events.add(toEvent(entry));
			}
		}
		return events;
	}

	public static List<InvestmentEvent> toInvestmentEvent(
			List<EventViewModel> eventsViewModel) {
		List<InvestmentEvent> events = new ArrayList<>();
		if (eventsViewModel != null) {
			for (EventViewModel entry : eventsViewModel) {
				events.add(toInvestmentEvent(entry));
			}
		}
		return events;
	}

	public static Investment toInvestment(
			InvestmentViewModel investmentViewModel) {
		String investmentName = investmentViewModel.getName();
		Integer investmentId = investmentViewModel.getId();
		Money principal = investmentViewModel.getPrincipal();
		Double interestRate = investmentViewModel.getInterestRate();
		Integer term = investmentViewModel.getTerm();
		Money monthlyContribution = investmentViewModel.getMonthlyContribution();
		List<InvestmentEvent> events = toInvestmentEvent(
				investmentViewModel.getEvents());
		Investment investment = new Investment(principal, interestRate, term,
				monthlyContribution, events);
		investment.setId(investmentId);
		investment.setName(investmentName);
		return investment;
	}

	public static Investment toInvestment(
			InvestmentDataViewModel InvestmentDataViewModel) {
		Money principal = InvestmentDataViewModel.getPrincipal();
		Double interestRate = InvestmentDataViewModel.getInterestRate();
		Integer term = InvestmentDataViewModel.getTerm();
		Money monthlyContribution = InvestmentDataViewModel
				.getMonthlyContribution();
		List<InvestmentEvent> events = toInvestmentEvent(
				InvestmentDataViewModel.getEvents());
		Integer investmentId = InvestmentDataViewModel.getId();
		String investmentName = InvestmentDataViewModel.getName();
		Investment investment = new Investment(principal, interestRate, term,
				monthlyContribution, events);
		investment.setId(investmentId);
		investment.setName(investmentName);
		return investment;
	}

	public static Customer toCustomer(CustomerViewModel customerViewModel) {
		Integer id = customerViewModel.getId();
		Integer customerNum = customerViewModel.getCustomerNum();
		String name = customerViewModel.getName();
		Customer customer = new Customer(customerNum, name);
		customer.setId(id);
		return customer;
	}

	public static PropertyBond toPropertyBond(PropertyBondViewModel model) {
		List<PropertyBondEvent> events = model.getEvents();
		Double interestRate = model.getInterestRate();
		Money principal = model.getPrincipal();
		Integer term = model.getTerm();
		return new PropertyBond(term, interestRate, principal, events);
	}

	public static PropertyBond toPropertyBond(
			PropertyBondDataViewModel propertyBondData) {
		List<PropertyBondEvent> events = propertyBondData.getEvents();
		Double interestRate = propertyBondData.getInterestRate();
		Money principal = propertyBondData.getPrincipal();
		Integer term = propertyBondData.getTerm();
		return new PropertyBond(term, interestRate, principal, events);
	}
	
}