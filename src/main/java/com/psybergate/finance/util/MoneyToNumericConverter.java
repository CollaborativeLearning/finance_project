package com.psybergate.finance.util;

import java.math.BigDecimal;

import javax.persistence.AttributeConverter;
import javax.persistence.Converter;

import com.psybergate.finance.domain.Money;

@Converter
public class MoneyToNumericConverter
		implements
			AttributeConverter<Money, BigDecimal> {

	@Override
	public BigDecimal convertToDatabaseColumn(Money money) {
		if (money == null) return null;
		return money.getAmount();
	}

	@Override
	public Money convertToEntityAttribute(BigDecimal dbData) {
		return new Money(dbData);
	}

}
