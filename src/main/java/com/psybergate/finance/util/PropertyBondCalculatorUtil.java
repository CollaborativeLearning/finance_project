package com.psybergate.finance.util;

import java.math.BigDecimal;
import java.util.Map;
import java.util.Map.Entry;

import com.psybergate.finance.domain.PropertyBondEvent;
import com.psybergate.finance.domain.Money;
import com.psybergate.finance.interfaces.ForecastEntry;

import java.util.SortedMap;
import java.util.TreeMap;

public class PropertyBondCalculatorUtil {

	private static final SortedMap<Integer, int[]> CONVEYANCE_TABLE;

	private static final SortedMap<Integer, int[]> DUTY_TABLE;

	private static final Double VAT_RATE = 0.15;

	static {
		CONVEYANCE_TABLE = new TreeMap<>();
		CONVEYANCE_TABLE.put(0, new int[]{5_000, 0, 0});
		CONVEYANCE_TABLE.put(100_000, new int[]{5_000, 50_000, 770});
		CONVEYANCE_TABLE.put(500_000, new int[]{11_160, 100_000, 1_540});
		CONVEYANCE_TABLE.put(1_000_000, new int[]{18_860, 200_000, 1_540});
		CONVEYANCE_TABLE.put(5_000_000, new int[]{49_660, 500_000, 1_925});

		DUTY_TABLE = new TreeMap<>();
		DUTY_TABLE.put(0, new int[]{0, 0});
		DUTY_TABLE.put(900_001, new int[]{0, 3});
		DUTY_TABLE.put(1_250_001, new int[]{10_500, 6});
		DUTY_TABLE.put(1_750_001, new int[]{40_500, 8});
		DUTY_TABLE.put(2_250_001, new int[]{80_500, 11});
		DUTY_TABLE.put(10_000_001, new int[]{933_000, 13});

	}

	public static Money getConveyanceFees(Money principal) {
		Entry<Integer, int[]> entry = getEntry(principal, CONVEYANCE_TABLE);
		if (entry.getKey() == CONVEYANCE_TABLE.firstKey()) {
			return new Money(entry.getValue()[0]);
		}
		BigDecimal lowerBound = BigDecimal.valueOf(entry.getKey());
		BigDecimal flatAmount = BigDecimal.valueOf(entry.getValue()[0]);
		BigDecimal divisor = BigDecimal.valueOf(entry.getValue()[1]);
		BigDecimal multiplicand = BigDecimal.valueOf(entry.getValue()[2]);
		BigDecimal remainder = principal.getAmount().subtract(lowerBound)
				.divide(divisor).setScale(0, BigDecimal.ROUND_CEILING);
		return new Money(flatAmount.add(multiplicand.multiply(remainder)));
	}

	public static Money getTransferDutyFees(Money principal) {
		Entry<Integer, int[]> entry = getEntry(principal, DUTY_TABLE);
		if (entry.getKey() == DUTY_TABLE.firstKey()) {
			return new Money();
		}
		BigDecimal lowerBound = BigDecimal.valueOf(entry.getKey() - 1);
		BigDecimal flatAmount = BigDecimal.valueOf(entry.getValue()[0]);
		BigDecimal percentage = BigDecimal.valueOf(entry.getValue()[1] / 100d);
		BigDecimal billableAmount = principal.getAmount().subtract(lowerBound)
				.multiply(percentage);
		return new Money(flatAmount.add(billableAmount));
	}

	public static Money getConveyanceVatFees(Money principal) {
		return getConveyanceFees(principal).multiply(VAT_RATE);
	}

	private static Map.Entry<Integer, int[]> getEntry(Money principal,
			SortedMap<Integer, int[]> table) {
		Double principalVal = principal.getAmount().doubleValue();
		Map.Entry<Integer, int[]> currentEntry = table.entrySet().iterator()
				.next();
		for (Map.Entry<Integer, int[]> entry : table.entrySet()) {
			Integer lowerBound = entry.getKey();
			if (principalVal < lowerBound) {
				return currentEntry;
			}
			else {
				currentEntry = entry;
			}
		}
		return currentEntry;
	}

	public static Money getMonthlyRepayment(Double interestRate, Integer term,
			Money principal) {
		if (interestRate == null || term == null || principal == null) {
			return null;
		}
		Double actualInterestRate = interestRate / 1200;
		Double interestFactor = Math.pow(1 + actualInterestRate, -term);
		Double denominator = 1 - interestFactor;
		Double repayment = principal.getAmount().doubleValue()
				* (actualInterestRate / denominator);
		return new Money(repayment);
	}

	public static Money getNewMonthlyRepayment(ForecastEntry entry, PropertyBondEvent event,
			Integer newTerm) {
		Double newInterestRate = entry.getInterestRate();
		Money newPrincipal = entry.getOpeningBalance();
		if (event.interestRateChanged()) {
			newInterestRate = event.getInterestRate();
		}
		if (event.additionalAmountChanged()) {
			newPrincipal = newPrincipal.subtract(event.getAmount());
		}
		else if (entry.getAmount() != null) {
			newPrincipal = newPrincipal.subtract(entry.getAmount());
		}
		Money newMonthlyRepayment = getMonthlyRepayment(newInterestRate, newTerm,
				newPrincipal);
		return newMonthlyRepayment;
	}

}
