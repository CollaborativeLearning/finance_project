package com.psybergate.finance.domain;

import static org.junit.Assert.assertEquals;

import org.junit.Test;

public class PropertyForecastEntryTest {

	private PropertyForecastEntry propertyForecastEntry;

	@Test
	public void testGenerateNextEntry() {
		propertyForecastEntry = new PropertyForecastEntry(1, 12.5, new Money(1000000), new Money(11361.41), new Money());
		assertEquals(new Money(999055.26), propertyForecastEntry.getClosingBalance());

		propertyForecastEntry = propertyForecastEntry.generateNextEntry();
		assertEquals(new Money(998100.68), propertyForecastEntry.getClosingBalance());
	}

	@Test
	public void testGetInterest() {
		Money expected;
		Money calculatedInterest;

		propertyForecastEntry = new PropertyForecastEntry(1, 10d, new Money(1000000), new Money(0), new Money());
		expected = new Money(8333.34);
		calculatedInterest = propertyForecastEntry.getInterest();
		assertEquals(expected, calculatedInterest);

		propertyForecastEntry = new PropertyForecastEntry(1, 12.5, new Money(1234567), new Money(0), new Money());
		expected = new Money(12860.08);
		calculatedInterest = propertyForecastEntry.getInterest();
		assertEquals(expected, calculatedInterest);
	}

	@Test
	public void testGetClosingBalance() {
		Money expected;
		Money closingBalance;

		propertyForecastEntry = new PropertyForecastEntry(1, 12.5, new Money(1000000), new Money(11361.41), new Money());
		expected = new Money(999055.26);
		closingBalance = propertyForecastEntry.getClosingBalance();
		assertEquals(expected, closingBalance);

		propertyForecastEntry = new PropertyForecastEntry(1, 15d, new Money(3000000), new Money(25000), new Money());
		expected = new Money(3012500.00);
		closingBalance = propertyForecastEntry.getClosingBalance();
		assertEquals(expected, closingBalance);
	}

	@Test
	public void testApplyEvent() {
		propertyForecastEntry = new PropertyForecastEntry(1, 12.5, new Money(1000000), new Money(11361.41), new Money());
		PropertyBondEvent event = new PropertyBondEvent(1, 12.0, new Money(5), new Money(20000));

		propertyForecastEntry.applyEvent(event);

		assertEquals(12.0, propertyForecastEntry.getInterestRate(), 0);
		assertEquals(new Money(5), propertyForecastEntry.getAmount());
		assertEquals(new Money(20000), propertyForecastEntry.getMonthlyAmount());
	}
}
