package com.psybergate.finance.domain;

import static org.junit.Assert.assertEquals;

import org.junit.Test;

import com.psybergate.finance.domain.Money;
import com.psybergate.finance.util.PropertyBondCalculatorUtil;

public class PropertyBondCalculatorUtilTest {

	@Test
	public void testGetTransferDuty() {
		Money principal = new Money(800_000);
		Money expected = new Money(0.0);
		Money actual = PropertyBondCalculatorUtil.getTransferDutyFees(principal);
		assertEquals(expected, actual);

		principal = new Money(2_000_000);
		expected = new Money(60_500);
		actual = PropertyBondCalculatorUtil.getTransferDutyFees(principal);
		assertEquals(expected, actual);

		principal = new Money(12_000_000);
		expected = new Money(1_193_000);
		actual = PropertyBondCalculatorUtil.getTransferDutyFees(principal);
		assertEquals(expected, actual);
	}

	@Test
	public void testGetConveyanceFees() {
		Money principal;
		Money expected;
		Money conveyanceFees;

		principal = new Money(500_000);
		expected = new Money(11_160);
		conveyanceFees = PropertyBondCalculatorUtil.getConveyanceFees(principal);
		assertEquals(expected, conveyanceFees);

		principal = new Money(600_000);
		expected = new Money(12_700);
		conveyanceFees = PropertyBondCalculatorUtil.getConveyanceFees(principal);
		assertEquals(expected, conveyanceFees);

		principal = new Money(550_000);
		expected = new Money(12_700);
		conveyanceFees = PropertyBondCalculatorUtil.getConveyanceFees(principal);
		assertEquals(expected, conveyanceFees);

		principal = new Money(6_000_000);
		expected = new Money(53_510);
		conveyanceFees = PropertyBondCalculatorUtil.getConveyanceFees(principal);
		assertEquals(expected, conveyanceFees);
	}

}
