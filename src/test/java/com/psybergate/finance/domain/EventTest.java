package com.psybergate.finance.domain;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

import org.junit.Before;
import org.junit.Test;

public class EventTest {

	private static final Integer DEFAULT_MONTH = 1;

	private static final Double DEFAULT_INTEREST_RATE = Double.valueOf("10.0");

	private static final Money DEFAULT_AMOUNT = new Money("1000.00");

	private static final Money DEFAULT_MONTHLY_AMOUNT = new Money("11000.00");

	private PropertyBondEvent event;

	@Before
	public void setDefaultTestValues() {
		event = new PropertyBondEvent(DEFAULT_MONTH, DEFAULT_INTEREST_RATE, DEFAULT_AMOUNT,
				DEFAULT_MONTHLY_AMOUNT);
	}

	@Test
	public void testGetMonth() {
		Integer month = event.getMonth().intValue();
		assertEquals(DEFAULT_MONTH, month);
	}

	@Test
	public void testSetMonth() {
		Integer month = 10;
		event.setMonth(month);
		Integer month1 = event.getMonth().intValue();

		assertEquals(month, month1);
	}

	@Test
	public void testInterestRateChangedReturnsTrueWhenInterestIsNotZero() {
		event.setInterestRate(Double.valueOf(12));
		boolean interestRateHasChanged = event.interestRateChanged();
		assertTrue(interestRateHasChanged);
	}

	@Test
	public void testInterestRateChangedReturnsFalseWhenInterestIsNull() {
		event.setInterestRate(null);
		boolean interestRateHasChanged = event.interestRateChanged();
		assertFalse(interestRateHasChanged);
	}

	@Test
	public void testMonthlyAmountChangedReturnsTrueWhenMonthlyAmountIsNotNull() {
		event.setMonthlyAmount(new Money("1000"));
		boolean monthlyAmountHasChanged = event.monthlyAmountChanged();
		assertTrue(monthlyAmountHasChanged);
	}

	@Test
	public void testMonthlyAmountChangedReturnsFalseWhenMonthlyAmountIsNull() {
		event.setMonthlyAmount(null);
		boolean monthlyAmountHasChanged = event.monthlyAmountChanged();
		assertFalse(monthlyAmountHasChanged);
	}

	@Test
	public void testGetInterestRate() {
		Double interestRate = event.getInterestRate();
		assertEquals(DEFAULT_INTEREST_RATE, interestRate);
	}

	@Test
	public void testSetInterestRate() {
		event.setInterestRate(Double.valueOf("90"));
		assertEquals(Double.valueOf("90"), event.getInterestRate());
	}

	@Test
	public void testGetAmount() {
		Money amount = event.getAmount();
		assertEquals(DEFAULT_AMOUNT, amount);
	}

	@Test
	public void testSetAmount() {
		Money depositOrWithdrawal = new Money(200);
		event.setAmount(depositOrWithdrawal);
		assertEquals(depositOrWithdrawal, event.getAmount());
	}

	@Test
	public void testGetMonthlyAmount() {
		Money monthlyAmount = event.getMonthlyAmount();
		assertEquals(DEFAULT_MONTHLY_AMOUNT, monthlyAmount);
	}

	@Test
	public void testSetMonthlyAmount() {
		Money monthlyAmount = new Money(20000);
		event.setMonthlyAmount(monthlyAmount);
		assertEquals(monthlyAmount, event.getMonthlyAmount());
	}

}
