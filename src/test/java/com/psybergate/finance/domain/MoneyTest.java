package com.psybergate.finance.domain;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

import org.junit.Test;

public class MoneyTest {

	private Money zeroMoney = new Money(0);

	@Test
	public void testAdd() {
		Money sum;

		zeroMoney = new Money(0);
		sum = zeroMoney.add(5);
		assertEquals(new Money(5), sum);

		sum = zeroMoney.add(-5);
		assertEquals(new Money(-5), sum);
	}

	@Test
	public void testSubtract() {
		Money difference;

		difference = zeroMoney.subtract(5);
		assertEquals(new Money(-5), difference);

		difference = zeroMoney.subtract(-5);
		assertEquals(new Money(5), difference);
	}

	@Test
	public void testMultiply() {
		Money product;
		Money unitMoney = new Money(1);

		product = unitMoney.multiply(6);
		assertEquals(new Money(6), product);

		product = unitMoney.multiply(0.333333);
		assertEquals(new Money("0.34"), product);
	}

	@Test
	public void testDivide() {
		Money quotient;
		Money money = new Money(5);

		quotient = money.divide(0.5);
		assertEquals(new Money(10), quotient);

		quotient = money.divide(1);
		assertEquals(new Money(5), quotient);

		quotient = money.divide(2);
		assertEquals(new Money(2.5), quotient);

		quotient = money.divide(4);
		assertEquals(new Money(1.25), quotient);
	}

	@Test
	public void testCompareTo() {
		Money money;

		money = new Money(3);
		boolean lessMoneyComparesLess = zeroMoney.compareTo(money) < 0;
		assertTrue(lessMoneyComparesLess);

		money = new Money(0);
		boolean equalMoneyComparesEqual = zeroMoney.compareTo(new Money(0)) == 0;
		assertTrue(equalMoneyComparesEqual);

		money = new Money(-32);
		boolean moreMoneyComparesGreater = zeroMoney
				.compareTo(new Money(-32)) > 0;
		assertTrue(moreMoneyComparesGreater);
	}

	@Test
	public void testEquals() {
		boolean bothMoniesAreEqual;

		Money negativeMoney1 = new Money("-2.35");
		Money negativeMoney2 = new Money("-2.35");
		bothMoniesAreEqual = negativeMoney1.equals(negativeMoney2);
		assertTrue(bothMoniesAreEqual);

		Money zeroMoney1 = new Money("0");
		Money zeroMoney2 = new Money("0");
		bothMoniesAreEqual = zeroMoney1.equals(zeroMoney2);
		assertTrue(bothMoniesAreEqual);

		Money positiveMoney1 = new Money("2.35");
		Money positiveMoney2 = new Money("2.35");
		bothMoniesAreEqual = positiveMoney1.equals(positiveMoney2);
		assertTrue(bothMoniesAreEqual);
	}

}
