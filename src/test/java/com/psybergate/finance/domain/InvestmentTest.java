package com.psybergate.finance.domain;

import static org.junit.Assert.assertEquals;

import java.util.ArrayList;
import java.util.List;

import org.junit.Test;

public class InvestmentTest {

	private Investment investment;

	@Test
	public void testGetFutureValue() {
		Money principal = new Money("10000");
		Double interestRate = 10d;
		Integer term = 10;
		Money monthlyContribution = new Money();
		List<InvestmentEvent> events = new ArrayList<>();

		investment = new Investment(principal, interestRate, term,
				monthlyContribution, events);
		assertEquals(new Money("10865.33"), investment.getFutureValue());

		principal = new Money("1000");
		investment = new Investment(principal, interestRate, term,
				monthlyContribution, events);
		assertEquals(new Money("1086.58"), investment.getFutureValue());

		term = 80;
		investment = new Investment(principal, interestRate, term,
				monthlyContribution, events);
		assertEquals(new Money("1942.93"), investment.getFutureValue());
	}

	@Test
	public void testGetTotalInterest() {
		Money principal = new Money("1000");
		Double interestRate = 10.0;
		Integer term = 80;
		Money monthlyContribution = new Money();
		List<InvestmentEvent> events = new ArrayList<>();
		Money totalInterest;

		investment = new Investment(principal, interestRate, term,
				monthlyContribution, events);
		totalInterest = investment.getTotalInterest();
		assertEquals(new Money("942.93"), totalInterest);

		term = 10;
		investment = new Investment(principal, interestRate, term,
				monthlyContribution, events);
		totalInterest = investment.getTotalInterest();
		assertEquals(new Money("86.58"), totalInterest);
	}

	@Test
	public void testGenerateEntriesCreatesSameNumberOfEntriesAsTerm() {
		Money principal = new Money("1000");
		Double interestRate = 10.0;
		Integer term = 1;
		Money monthlyContribution = new Money();
		List<InvestmentEvent> events = new ArrayList<>();
		Integer numberOfEntries;

		investment = new Investment(principal, interestRate, term,
				monthlyContribution, events);
		investment.generateEntries();
		numberOfEntries = investment.getForecast().size();
		assertEquals(term, numberOfEntries);

		term = 2;
		investment = new Investment(principal, interestRate, term,
				monthlyContribution, events);
		investment.generateEntries();
		numberOfEntries = investment.getForecast().size();
		assertEquals(term, numberOfEntries);

		term = 80;
		investment = new Investment(principal, interestRate, term,
				monthlyContribution, events);
		investment.generateEntries();
		numberOfEntries = investment.getForecast().size();
		assertEquals(term, numberOfEntries);
	}
}
