package com.psybergate.finance.domain;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

import org.junit.Before;
import org.junit.Test;

import com.psybergate.finance.interfaces.ForecastEntry;

public class InvestmentForecastEntryTest {

	private static final Integer DEFAULT_MONTH = 1;

	private static final Money DEFAULT_OPENING_BALANCE = new Money("1000");

	private static final Double DEFAULT_INTEREST_RATE = 10d;

	private static final Money DEFAULT_AMOUNT = new Money();

	private static final Money DEFAULT_MONTHLY_AMOUNT = new Money();

	private ForecastEntry forecastEntry;

	@Before
	public void setDefaultTestValues() {
		forecastEntry = new InvestmentForecastEntry(DEFAULT_MONTH,
				DEFAULT_INTEREST_RATE, DEFAULT_OPENING_BALANCE,
				DEFAULT_MONTHLY_AMOUNT, DEFAULT_AMOUNT);
	}

	@Test
	public void testGetInterestReturnsExpectedInterestForSingleMonth() {
		Money calculatedInterest;

		forecastEntry = new InvestmentForecastEntry(1, 10d, new Money("1000"),
				new Money(), new Money());
		calculatedInterest = forecastEntry.getInterest();
		assertEquals(new Money("8.34"), calculatedInterest);

		forecastEntry = new InvestmentForecastEntry(2, 10d, new Money("1000"),
				new Money("300"), new Money());
		calculatedInterest = forecastEntry.getInterest();
		assertEquals(new Money("10.84"), calculatedInterest);

		forecastEntry = new InvestmentForecastEntry(2, 10d, new Money("1000"),
				new Money("300"), new Money("200"));
		calculatedInterest = forecastEntry.getInterest();
		assertEquals(new Money("12.50"), calculatedInterest);
	}

	@Test
	public void testGetClosingBalanceCalculatesAndReturnsCorrectClosingBalanceForSingleMonth() {
		Money closingBalance;

		Integer month = 1;
		Double interestRate = 10d;
		Money openingBalance = new Money("1000");
		Money monthlyAmount = new Money();
		Money amount = new Money();

		forecastEntry = new InvestmentForecastEntry(month, interestRate,
				openingBalance, monthlyAmount, amount);
		closingBalance = forecastEntry.getClosingBalance();
		assertEquals(new Money("1008.34"), closingBalance);

		monthlyAmount = new Money("300");
		forecastEntry = new InvestmentForecastEntry(month, interestRate,
				openingBalance, monthlyAmount, amount);
		closingBalance = forecastEntry.getClosingBalance();
		assertEquals(new Money("1310.84"), closingBalance);

		amount = new Money("200");
		forecastEntry = new InvestmentForecastEntry(month, interestRate,
				openingBalance, monthlyAmount, amount);
		closingBalance = forecastEntry.getClosingBalance();
		assertEquals(new Money("1512.50"), closingBalance);
	}

	@Test
	public void testCompareTo() {
		ForecastEntry firstForecastEntry = new InvestmentForecastEntry(1,
				DEFAULT_INTEREST_RATE, DEFAULT_OPENING_BALANCE,
				DEFAULT_MONTHLY_AMOUNT, DEFAULT_AMOUNT);

		ForecastEntry secondForecastEntry = new InvestmentForecastEntry(2,
				DEFAULT_INTEREST_RATE, DEFAULT_OPENING_BALANCE,
				DEFAULT_MONTHLY_AMOUNT, DEFAULT_AMOUNT);

		ForecastEntry thirdForecastEntry = new InvestmentForecastEntry(3,
				DEFAULT_INTEREST_RATE, DEFAULT_OPENING_BALANCE,
				DEFAULT_MONTHLY_AMOUNT, DEFAULT_AMOUNT);

		boolean earlierEntryComparesGreaterThanLaterEntry = secondForecastEntry
				.compareTo(thirdForecastEntry) < 0;
		assertTrue(earlierEntryComparesGreaterThanLaterEntry);

		boolean entriesInSameMonthCompareAsEquals = secondForecastEntry
				.compareTo(secondForecastEntry) == 0;
		assertTrue(entriesInSameMonthCompareAsEquals);

		boolean laterEntryComparesGreaterThanEarlierEntry = secondForecastEntry
				.compareTo(firstForecastEntry) > 0;
		assertTrue(laterEntryComparesGreaterThanEarlierEntry);
	}

	@Test
	public void testGetInterestRate() {
		Double interestRate = forecastEntry.getInterestRate();
		assertEquals(DEFAULT_INTEREST_RATE, interestRate, 0);
	}

	@Test
	public void testGetOpeningBalance() {
		Money openingBalance = forecastEntry.getOpeningBalance();
		assertEquals(DEFAULT_OPENING_BALANCE, openingBalance);
	}

	@Test
	public void testGetMonthlyContribution() {
		Money monthlyAmount = forecastEntry.getMonthlyAmount();
		assertEquals(DEFAULT_MONTHLY_AMOUNT, monthlyAmount);
	}

}
