package com.psybergate.finance.domain;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

import java.util.ArrayList;

import org.junit.Before;
import org.junit.Test;

public class PropertyBondTest {

	private PropertyBond bond;

	@Before
	public void setDefaultTestValues() {
		bond = new PropertyBond(240, 10.25, new Money(1_000_000),
				new ArrayList<>());
	}

	@Test
	public void testGetMonthlyRepaymentReturnsExpectedValuesForValidInputs() {
		Money expected;
		Money monthlyRepayment;

		bond = new PropertyBond(240, 10.25, new Money(1_000_000),
				new ArrayList<>());
		expected = new Money("9816.44");
		monthlyRepayment = bond.getMonthlyRepayment();
		assertEquals(expected, monthlyRepayment);

		bond = new PropertyBond(240, 10.25, new Money(1_200_000),
				new ArrayList<>());
		expected = new Money("11779.73");
		monthlyRepayment = bond.getMonthlyRepayment();
		assertEquals(expected, monthlyRepayment);

		bond = new PropertyBond(240, 12.5, new Money(1_200_000),
				new ArrayList<>());
		expected = new Money("13633.69");
		monthlyRepayment = bond.getMonthlyRepayment();
		assertEquals(expected, monthlyRepayment);
	}

	@Test
	public void testGetClosingBalanceReturnsZeroForValidBondInputs() {
		Money closingBalance;
		Money expectedZeroBalance = new Money(0);

		bond = new PropertyBond(240, 10.25, new Money(1_000_000),
				new ArrayList<>());
		closingBalance = bond.getClosingBalance();
		assertEquals(expectedZeroBalance, closingBalance);

		bond = new PropertyBond(240, 10.25, new Money(1_200_000),
				new ArrayList<>());
		assertEquals(expectedZeroBalance, closingBalance);

		bond = new PropertyBond(240, 12.5, new Money(1_200_000),
				new ArrayList<>());
		assertEquals(expectedZeroBalance, closingBalance);
	}

	@Test
	public void testGetTotalInterestReturnsExpectedValuesForValidInputs() {
		Money expected;
		Money totalInterest;

		bond = new PropertyBond(240, 10.25, new Money(1_000_000),
				new ArrayList<>());
		expected = new Money("1355944.78");
		totalInterest = bond.getTotalInterest();
		assertEquals(expected, totalInterest);

		bond = new PropertyBond(240, 10.25, new Money(1_200_000),
				new ArrayList<>());
		expected = new Money("1627131.63");
		totalInterest = bond.getTotalInterest();
		assertEquals(expected, totalInterest);

		bond = new PropertyBond(240, 12.5, new Money(1_200_000),
				new ArrayList<>());
		expected = new Money("2072087.01");
		totalInterest = bond.getTotalInterest();
		assertEquals(expected, totalInterest);
	}

	@Test
	public void testGetTotalRepaymentReturnsExpectedValuesForValidInputs() {
		Money expected;
		Money totalRepayment;

		bond = new PropertyBond(240, 10.25, new Money(1_000_000),
				new ArrayList<>());
		expected = new Money("2355944.78");
		totalRepayment = bond.getTotalRepayment();
		assertEquals(expected, totalRepayment);

		bond = new PropertyBond(240, 10.25, new Money(1_200_000),
				new ArrayList<>());
		expected = new Money("2827131.63");
		totalRepayment = bond.getTotalRepayment();
		assertEquals(expected, totalRepayment);

		bond = new PropertyBond(240, 12.5, new Money(1_200_000),
				new ArrayList<>());
		expected = new Money("3272087.01");
		totalRepayment = bond.getTotalRepayment();
		assertEquals(expected, totalRepayment);
	}

	@Test
	public void testGenerateEntriesCreatesSameNumberOfEntriesAsTerm() {
		Integer term;
		Integer numberOfEntries;

		bond = new PropertyBond(25, 10d, new Money(10000), new ArrayList<>());
		bond.generateEntries();
		assertFalse(bond.getForecast().isEmpty());

		term = 1;
		bond = new PropertyBond(term, 12.5, new Money(1_200_000),
				new ArrayList<>());
		numberOfEntries = bond.getForecast().size();
		assertEquals(term, numberOfEntries);

		term = 2;
		bond = new PropertyBond(term, 12.5, new Money(1_200_000),
				new ArrayList<>());
		numberOfEntries = bond.getForecast().size();
		assertEquals(term, numberOfEntries);

		term = 240;
		bond = new PropertyBond(term, 12.5, new Money(1_200_000),
				new ArrayList<>());
		numberOfEntries = bond.getForecast().size();
		assertEquals(term, numberOfEntries);
	}

	@Test
	public void testAddEvent() {
		bond = new PropertyBond(25, 10d, new Money(10000), new ArrayList<>());
		PropertyBondEvent event = new PropertyBondEvent();
		bond.addEvent(event);
		assertTrue(bond.getEvents().contains(event));
	}

}
